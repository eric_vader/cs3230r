Review of CPM and COS
=====================

This is a NUS-CS3230R project.

------------------------------------------------------------------------

Introduction
------------
The project compares the 2 algorithms, Clique Percolation Method (CPM) of Palla et. al. and CPM On Steroids (COS) by S Mainardi et. al. COS is a parallel version of the clique percolation method.

Listing of Required items for submission
----------------------------------------
 * Code including tests, are in `<PROJ DIR>/src/main` and `<PROJ DIR>/src/test`.
 * Scripts are included in `<PROJ DIR>/src/main/resources`.
 * Datasets included here and in report.
 * Result files are included in `<PROJ DIR>/results`.
 * README.md, this document in `<PROJ DIR>/README.md`.
 * HOWTO.md, included in `<PROJ DIR>/HOWTO.md`.

### Author
Han Liang Wee, Eric(A0065517A)

### Code License
All source code and bash script are licensed GNU GENERAL PUBLIC LICENSE, Version 3, 29 June 2007. See `LICENSE` for the full license.

### Project Management
The project is managed by [Apache Maven](https://maven.apache.org/). Apache Maven is a software management and comprehension tool.

### Language
This project is written in the Java programming Language, JDK 8.

------------------------------------------------------------------------

Project Listing
---------------
This section will chronicle the files listing and the external software dependencies of the project.

### Listing of Files
There are 43 directories, 79 files in this project. The project adheres to [Maven standard Directory Layout](https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html). The complete listing(`$ tree`) of the files in the project is as follows:
```
├── googleCheckstyle.xml
├── log
├── pom.xml
├── README.md
└── src
    ├── main
    │   ├── java
    │   │   └── com
    │   │       └── e2empire
    │   │           ├── algorithm
    │   │           │   ├── Algorithm.java
    │   │           │   ├── AlgorithmResult.java
    │   │           │   └── graph
    │   │           │       ├── community
    │   │           │       │   ├── CliquePercolationMethodData.java
    │   │           │       │   ├── CliquePercolationMethod.java
    │   │           │       │   ├── CliquePercolationMethodParallelData.java
    │   │           │       │   ├── OptimizedCliquePercolationMethod.java
    │   │           │       │   └── ParallelCommunityDetection.java
    │   │           │       └── maximalcliques
    │   │           │           ├── BronKerbosch.java
    │   │           │           ├── BronKerboschVertexOrdering.java
    │   │           │           ├── BronKerboschWithoutPivot.java
    │   │           │           ├── BronKerboschWithPivot.java
    │   │           │           └── MaximalCliquesAlgorithm.java
    │   │           ├── App.java
    │   │           ├── data
    │   │           │   ├── DataStructure.java
    │   │           │   ├── graph
    │   │           │   │   ├── Edge.java
    │   │           │   │   ├── GraphBuilder.java
    │   │           │   │   ├── Graph.java
    │   │           │   │   ├── GraphTree.java
    │   │           │   │   ├── Set.java
    │   │           │   │   ├── SizeAscendingComparator.java
    │   │           │   │   ├── VertexComparator.java
    │   │           │   │   └── Vertex.java
    │   │           │   └── set
    │   │           │       ├── MergingUnionFind.java
    │   │           │       ├── NonMergingUnionFind.java
    │   │           │       ├── UnionFindConfig.java
    │   │           │       └── UnionFind.java
    │   │           └── FileUtil.java
    │   └── resources
    │       ├── graph
    │       │   ├── real
    │       │   │   ├── adjnoun
    │       │   │   │   ├── adjnoun.gml
    │       │   │   │   └── adjnoun.txt
    │       │   │   ├── as-22july06
    │       │   │   │   ├── as-22july06.gml
    │       │   │   │   └── as-22july06.txt
    │       │   │   ├── astro-ph
    │       │   │   │   ├── astro-ph.gml
    │       │   │   │   └── astro-ph.txt
    │       │   │   ├── celegansneural
    │       │   │   │   ├── celegansneural.gml
    │       │   │   │   └── celegansneural.txt
    │       │   │   ├── cond-mat
    │       │   │   │   ├── cond-mat.gml
    │       │   │   │   └── cond-mat.txt
    │       │   │   ├── cond-mat-2003
    │       │   │   │   ├── cond-mat-2003.gml
    │       │   │   │   └── cond-mat-2003.txt
    │       │   │   ├── cond-mat-2005
    │       │   │   │   ├── cond-mat-2005.gml
    │       │   │   │   └── cond-mat-2005.txt
    │       │   │   ├── dolphins
    │       │   │   │   ├── dolphins.gml
    │       │   │   │   └── dolphins.txt
    │       │   │   ├── football
    │       │   │   │   ├── football.gml
    │       │   │   │   └── football.txt
    │       │   │   ├── hep-th
    │       │   │   │   ├── hep-th.gml
    │       │   │   │   └── hep-th.txt
    │       │   │   ├── karate
    │       │   │   │   ├── karate.gml
    │       │   │   │   └── karate.txt
    │       │   │   ├── lesmis
    │       │   │   │   ├── lesmis.gml
    │       │   │   │   └── lesmis.txt
    │       │   │   ├── netscience
    │       │   │   │   ├── netscience.gml
    │       │   │   │   └── netscience.txt
    │       │   │   ├── polblogs
    │       │   │   │   ├── polblogs.gml
    │       │   │   │   └── polblogs.txt
    │       │   │   ├── polbooks
    │       │   │   │   ├── polbooks.gml
    │       │   │   │   └── polbooks.txt
    │       │   │   └── power
    │       │   │       ├── power.gml
    │       │   │       └── power.txt
    │       │   └── synthetic
    │       ├── log4j.properties
    │       ├── README.md
    │       └── run.sh
    └── test
        └── java
            └── com
                └── e2empire
                    ├── algorithm
                    │   └── graph
                    │       ├── community
                    │       │   ├── CliquePercolationMethodParallelTest.java
                    │       │   └── CliquePercolationMethodTest.java
                    │       └── maximalcliques
                    │           ├── BronKerboschTest.java
                    │           ├── BronKerboschVertexOrderingTest.java
                    │           ├── BronKerboschWithoutPivotTest.java
                    │           └── BronKerboschWithPivotTest.java
                    └── data
                        └── graph
                            ├── Common.java
                            ├── EdgeEqualityTest2.java
                            ├── EdgeEqualityTest.java
                            ├── EdgeHashcodeTest.java
                            ├── GraphTest.java
                            ├── VertexEqualityTest.java
                            ├── VertexHashcodeTest.java
                            └── VertexSortTest.java
```

### Listing of External Dependencies
Since the project is managed by Maven, the software dependencies will all be installed automatically by Maven when it is building the program. There is no need to install any of them manually. The listing of software dependencies(`$ mvn dependency:tree`) are as follows:
```
+- com.tinkerpop.blueprints:blueprints-core:jar:2.6.0:compile
|  +- org.codehaus.jettison:jettison:jar:1.3.3:compile
|  |  \- stax:stax-api:jar:1.0.1:compile
|  +- com.fasterxml.jackson.core:jackson-databind:jar:2.2.3:compile
|  |  +- com.fasterxml.jackson.core:jackson-annotations:jar:2.2.3:compile
|  |  \- com.fasterxml.jackson.core:jackson-core:jar:2.2.3:compile
|  +- com.carrotsearch:hppc:jar:0.6.0:compile
|  +- commons-configuration:commons-configuration:jar:1.6:compile
|  |  +- commons-collections:commons-collections:jar:3.2.1:compile
|  |  +- commons-lang:commons-lang:jar:2.4:compile
|  |  +- commons-digester:commons-digester:jar:1.8:compile
|  |  |  \- commons-beanutils:commons-beanutils:jar:1.7.0:compile
|  |  \- commons-beanutils:commons-beanutils-core:jar:1.8.0:compile
|  \- commons-logging:commons-logging:jar:1.1.1:compile
+- org.slf4j:slf4j-log4j12:jar:1.7.12:compile
|  +- org.slf4j:slf4j-api:jar:1.7.12:compile
|  \- log4j:log4j:jar:1.2.17:compile
+- org.apache.commons:commons-collections4:jar:4.0:compile
+- junit:junit:jar:4.12:test
|  \- org.hamcrest:hamcrest-core:jar:1.3:test
+- com.googlecode.javaewah:JavaEWAH:jar:1.0.2:compile
\- com.googlecode.princeton-java-algorithms:algorithms:jar:4.0.1:compile
   +- com.googlecode.princeton-java-introduction:stdlib:jar:1.0.1:compile
   |  \- java3d:vecmath:jar:1.3.1:compile
   +- java3d:j3d-core-utils:jar:1.3.1:compile
   |  \- java3d:j3d-core:jar:1.3.1:compile
   \- gov.nist.math:jama:jar:1.0.3:compile
```

### Listing of Project Dependencies
Project dependencies are what one needs to install to compile and run the project. The listing of project dependencies are as follows:

1. Java JDK8
> Tested on:
>
> 1. OpenJDK Java(build 1.8.0_51-b16) 64-Bit Server VM (build 25.51-b03, mixed mode)
> 1. OracleJDK Java(TM)(build 1.8.0_60-b27) HotSpot(TM) 64-Bit Server VM (build 25.60-b23, mixed mode)
2. Apache Maven 3
> Tested on:
> 
> 1. Apache Maven 3.2.5 with OpenJDK 1.8.0_51
> 1. Apache Maven 3.0.5 (Red Hat 3.0.5-16) with OracleJDK 1.8.0_60

### Listing of Datasets
The dataset that is used in testing of the algorithms are a subset from [Network data](http://www-personal.umich.edu/~mejn/netdata/). These datasets are in the [GML format](http://www.infosun.fmi.uni-passau.de/Graphlet/GML/gml-tr.html), which is a common graph representation standard. These graphs are all collections of non-synthetic experimental data. The descriptions below are taken from [Network data](http://www-personal.umich.edu/~mejn/netdata/).

| Dataset Name  | Number of Vertices             | Number of Edges             | Description             | Citation             |
| ------------- | ------------------------------ | ------------------------------ | ------------------------------ | ------------------------------ |
| karate   | 34 |  78 | social network of friendships between 34 members of a karate club at a US university in the 1970s. | W. W. Zachary, An information flow model for conflict and fission in small groups, Journal of Anthropological Research 33, 452-473 (1977)|
| dolphins | 62 | 159 | an undirected social network of frequent associations between 62 dolphins in a community living off Doubtful Sound, New Zealand. | D. Lusseau, K. Schneider, O. J. Boisseau, P. Haase, E. Slooten, and S. M. Dawson, Behavioral Ecology and Sociobiology 54, 396-405 (2003)|
| lemis | 77 | 254 | coappearance network of characters in the novel Les Miserables. | D. E. Knuth, The Stanford GraphBase: A Platform for Combinatorial Computing, Addison-Wesley, Reading, MA (1993) |
| adjnoun | 112 | 425 | adjacency network of common adjectives and nouns in the novel David Copperfield by Charles Dickens. | M. E. J. Newman, Phys. Rev. E 74, 036104 (2006) |
| polbooks | 105 | 441 | A network of books about US politics published around the time of the 2004 presidential election and sold by the online bookseller Amazon.com. Edges between books represent frequent copurchasing of books by the same buyers. | V. Krebs, unpublished, http://www.orgnet.com/ |
| netscience | 1589 | 2742 | coauthorship network of scientists working on network theory and experiment, as compiled by M. Newman in May 2006 | M. E. J. Newman, Phys. Rev. E 74, 036104 (2006) |
| power | 4941 | 6594 | An undirected, unweighted network representing the topology of the Western States Power Grid of the United States | D. J. Watts and S. H. Strogatz, Nature 393, 440-442 (1998) |
| hep-th | 8361 | 15751 | weighted network of coauthorships between scientists posting preprints on the High-Energy Theory E-Print Archive between Jan 1, 1995 and December 31, 1999 | M. E. J. Newman, Proc. Natl. Acad. Sci. USA 98, 404-409 (2001) |
| as-22july06 | 22963 | 48436 | a symmetrized snapshot of the structure of the Internet at the level of autonomous systems, reconstructed from BGP tables posted by the University of Oregon Route Views Project | Mark Newman, unpublished, http://www-personal.umich.edu/~mejn/netdata/ |
| cond-mat | 16726 | 47594 | weighted network of coauthorships between scientists posting preprints on the Condensed Matter E-Print Archive between Jan 1, 1995 and December 31, 1999 | M. E. J. Newman, The structure of scientific collaboration networks, Proc. Natl. Acad. Sci. USA 98, 404-409 (2001) |
| astro-ph | 16706  | 121251 | weighted network of coauthorships between scientists posting preprints on the Astrophysics E-Print Archive between Jan 1, 1995 and December 31, 1999 | M. E. J. Newman, Proc. Natl. Acad. Sci. USA 98, 404-409 (2001) |
| cond-mat-2003 | 31163 | 120029 | updated network of coauthorships between scientists posting preprints on the Condensed Matter E-Print Archive | M. E. J. Newman, Proc. Natl. Acad. Sci. USA 98, 404-409 (2001) |
| cond-mat-2005 | 40421 | 175692 | updated network of coauthorships between scientists posting preprints on the Condensed Matter E-Print Archive | M. E. J. Newman, Proc. Natl. Acad. Sci. USA 98, 404-409 (2001) |

Acknowledgement
---------------
The author wishes to thank the aforementioned authors for their data and the creators of the software libraries that made this project possible.