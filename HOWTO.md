Installation Guide
==================
This installation guide is tailored for RHEL Linux Systems. The commands should work for any RHEL linux.

Note
----
The package manager `yum` is being deprecated, replacing it with `dnf`. In this guide, we will use the command `dnf`. However, `dnf` commands are syntactically and semantically similar and can be interchanged.

Install and run
---------------
1. Install the project dependencies
1. Build the project
1. Run the desired tests


Installing the Project Dependencies
-----------------------------------

1. Install OpenJDK from repository.
```
# dnf install java-1.8.0-openjdk
```
2. Install Maven from repository. Be sure to install maven 3.
```
# dnf install maven
```
3. Ensure that both maven and java are installed by running the commands:
  1. `mvn -version`
  1. `java -version`

Build the Project
-----------------
1. Build the project using maven `mvn clean install -U`
1. Ensure the Build was successful, an example of the successful output is given below:
```
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 17.389s
[INFO] Finished at: Mon Oct 12 17:37:18 SGT 2015
[INFO] Final Memory: 71M/1024M
[INFO] ------------------------------------------------------------------------
```

Run the Test
------------
1. Run the script `sh ./classes/run.sh <PATH TO DIR>/target/classes/graph/real`, an example is ` sh ./classes/run.sh /opt/cs3230r/target/classes/graph/real`

> _Note_
> * The Graph must be of the same directory as given in this repository for the script to work.
> * The script will run through all the algorithms for all the graphs found on the directory.