package com.e2empire.algorithm.graph.maximalcliques;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.e2empire.algorithm.graph.maximalcliques.BronKerbosch;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschVertexOrdering;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.List;

/**
 * Tests the Bron Kernosch algorithm without Pivot.
 *
 * @author Han Liang Wee, Eric
 *
 */
@RunWith(Parameterized.class)
public class BronKerboschVertexOrderingTest extends BronKerboschTest {

  /**
   * Initialize the Bron Kerbosh with vertex Ordering Test.
   *
   * @param graph
   * @param maximalCliquesExpected
   */
  public BronKerboschVertexOrderingTest(Graph graph, List<Vertices> maximalCliquesExpected) {
    super(graph, maximalCliquesExpected);
  }

  @Override
  public BronKerbosch getBronKerboschAlgorithm() {
    return new BronKerboschVertexOrdering();
  }
}
