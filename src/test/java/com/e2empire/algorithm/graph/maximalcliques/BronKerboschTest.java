package com.e2empire.algorithm.graph.maximalcliques;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Common;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Tests the Bron Kernosch algorithm without Pivot.
 *
 * @author Han Liang Wee, Eric
 *
 */
@RunWith(Parameterized.class)
public abstract class BronKerboschTest {

  /**
   * @return The algorithm that will be used to test.
   */
  public abstract BronKerbosch getBronKerboschAlgorithm();

  /**
   * @return Data used for the test.
   */
  @Parameters(name = "Test[{index}] Graph[{0}] should have Cliques[{1}]")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {Common.getGraphOnPaper(), Common.getMaximalCliquesOnPaper()},
        {Common.getGraphOnWiki(), Common.getMaximalCliquesOnWiki()}});
  }

  private List<Vertices> maximalCliquesExpected;
  private Graph graph;

  /**
   * Create a test with a graph and maximal Cliques
   *
   * @param graph graph to use to test.
   * @param maximalCliquesExpected The maximal cliques that is expected from the running of the
   *        algorithm.
   */
  public BronKerboschTest(Graph graph, List<Vertices> maximalCliquesExpected) {
    this.graph = graph;
    this.maximalCliquesExpected = maximalCliquesExpected;
  }

  /**
   * Get the maximum cliques for the graph and test it.
   */
  @Test
  public void testMaxCliques() {

    BronKerbosch bronKerboschAlgorithm = this.getBronKerboschAlgorithm();
    List<Vertices> maximalCliques = bronKerboschAlgorithm.execute(this.graph);

    // TODO fix the mysterious equals problem.
    Set<String> set = new HashSet<>();

    for (Vertices eaVertices : this.maximalCliquesExpected) {
      set.add(eaVertices.toString());
    }

    for (Vertices eaMaxCliques : maximalCliques) {
      assertTrue(set.contains(eaMaxCliques.toString()));
    }

  }

}
