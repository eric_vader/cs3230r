package com.e2empire.algorithm.graph.community;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.algorithm.graph.maximalcliques.BronKerboschVertexOrdering;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschWithPivot;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschWithoutPivot;
import com.e2empire.algorithm.graph.maximalcliques.MaximalCliquesAlgorithm;
import com.e2empire.data.graph.Common;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.set.MergingUnionFind;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Clique Percolation Method Test. This tests the CPM algorithm.
 *
 * @author Han Liang Wee, Eric
 *
 */
@RunWith(Parameterized.class)
public class CliquePercolationMethodTest {

  private OptimizedCliquePercolationMethod<MergingUnionFind<Vertex<?>, Vertices>> cpm;
  private int k;
  private List<Vertices> expectedKCliqueCommunity;
  private Graph graph;

  /**
   * @return The data used by the algorithm.
   */
  @Parameters(
      name = "Test[{index}] CPM[maxCliqueAlgo={0}, UFDataStructure={1}] on Graph[{4}] with expected {2}-clique community {3}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {new BronKerboschWithoutPivot(), MergingUnionFind.class, 3,
          Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper()},
          {new BronKerboschWithPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper()},
            {new BronKerboschVertexOrdering(), MergingUnionFind.class, 3,
              Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper()}});
  }

  /**
   * Initializes a new Clique Percolation Method algorithm.
   *
   * @param maximalCliquesAlgorithm Algorithm to find maximal cliques.
   * @param unionFindDataStructure The Union-Find data structure that is used.
   */
  public <T extends MergingUnionFind<Vertex<?>, Vertices>> CliquePercolationMethodTest(
      MaximalCliquesAlgorithm maximalCliquesAlgorithm, Class<T> unionFindDataStructure, int k,
      List<Vertices> expectedKCliqueCommunity, Graph graph) {
    this.cpm = new OptimizedCliquePercolationMethod(maximalCliquesAlgorithm, unionFindDataStructure);
    this.k = k;
    this.expectedKCliqueCommunity = expectedKCliqueCommunity;
    this.graph = graph;
  }

  @Test
  public void testCliquePercolationMethod() throws InstantiationException, IllegalAccessException {
    CliquePercolationMethodData config = new CliquePercolationMethodData();
    config.setIntK(this.k);
    config.setGraph(this.graph);

    // TODO fix mysterious equality problem with vertices
    Set<String> set = new HashSet<>();
    for (Vertices eaVerticesExpected : this.expectedKCliqueCommunity) {
      set.add(eaVerticesExpected.toString());
    }
    for (Vertices eaVerticesActual : this.cpm.execute(config)) {
      assertTrue(set.contains(eaVerticesActual.toString()));
    }
  }
}
