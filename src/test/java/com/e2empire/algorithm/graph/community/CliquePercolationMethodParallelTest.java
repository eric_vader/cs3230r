package com.e2empire.algorithm.graph.community;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.algorithm.graph.maximalcliques.BronKerboschVertexOrdering;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschWithPivot;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschWithoutPivot;
import com.e2empire.algorithm.graph.maximalcliques.MaximalCliquesAlgorithm;
import com.e2empire.data.graph.Common;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;
import com.e2empire.data.graph.GraphTree;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.set.MergingUnionFind;
import com.e2empire.data.set.NonMergingUnionFind;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Performs a COSpoc test.
 *
 * @author Han Liang Wee, Eric
 *
 */
@RunWith(Parameterized.class)
public class CliquePercolationMethodParallelTest {

  private ParallelCommunityDetection<MergingUnionFind<Vertex<?>, GraphTree.Vertices>> cpm;
  private int k;
  private List<Vertices> expectedKCliqueCommunity;
  private Graph graph;
  private int nProcs;

  /**
   * @return the data used in this test.
   */
  @Parameters(
      name = "Test[{index}] COSpoc[maxCliqueAlgo={0}, UFDataStructure={1}] on Graph[{4}] with expected {2}-clique community {3}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {
        {new BronKerboschWithoutPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschWithPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschVertexOrdering(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschWithoutPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 2},
        {new BronKerboschWithPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 3},
        {new BronKerboschVertexOrdering(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 4},
        {new BronKerboschWithoutPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 5},
        {new BronKerboschWithPivot(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 6},
        {new BronKerboschVertexOrdering(), MergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 7},
        {new BronKerboschWithoutPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschWithPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschVertexOrdering(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 1},
        {new BronKerboschWithoutPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 2},
        {new BronKerboschWithPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 3},
        {new BronKerboschVertexOrdering(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 4},
        {new BronKerboschWithoutPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 5},
        {new BronKerboschWithPivot(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 6},
        {new BronKerboschVertexOrdering(), NonMergingUnionFind.class, 3,
            Common.get3CliqueCommunityOnPaper(), Common.getGraphOnPaper(), 7}
        // end
    });
  }

  /**
   * Initializes a new Clique Percolation Method algorithm.
   *
   * @param maximalCliquesAlgorithm Algorithm to find maximal cliques.
   * @param unionFindDataStructure The Union-Find data structure that is used.
   * @param k
   * @param expectedKCliqueCommunity
   * @param graph
   * @param nProcs
   */
  public <T extends MergingUnionFind<Vertex<?>, Vertices>> CliquePercolationMethodParallelTest(
      MaximalCliquesAlgorithm maximalCliquesAlgorithm, Class<T> unionFindDataStructure, int k,
      List<Vertices> expectedKCliqueCommunity, Graph graph, int nProcs) {
    this.cpm = new ParallelCommunityDetection(maximalCliquesAlgorithm, unionFindDataStructure);
    this.k = k;
    this.expectedKCliqueCommunity = expectedKCliqueCommunity;
    this.graph = graph;
    this.nProcs = nProcs;
  }

  public static class NaturalOrderingComparator implements Comparator<Vertex<?>> {

    @Override
    public int compare(Vertex<?> o1, Vertex<?> o2) {
      String first = (String) o1.getElement();
      first = first.substring(1);
      String second = (String) o2.getElement();
      second = second.substring(1);
      return Integer.compare(Integer.parseInt(first), Integer.parseInt(second));
    }

  }

  @Test
  public void testCliquePercolationMethod() throws InstantiationException, IllegalAccessException {

    CliquePercolationMethodParallelData config = new CliquePercolationMethodParallelData();
    config.setIntK(this.k);
    config.setGraph(this.graph);
    config.setNumProcs(this.nProcs);

    // TODO fix mysterious equality problem with vertices
    Set<String> set = new HashSet<>();
    for (Vertices eaVerticesExpected : this.expectedKCliqueCommunity) {
      set.add(eaVerticesExpected.toString());
    }
    List<GraphTree.Vertices> listOfVertices = this.cpm.execute(config);
    for (GraphTree.Vertices eaVerticesActual : listOfVertices) {
      List<Vertex<?>> list = eaVerticesActual.toList();
      Collections.sort(list, new NaturalOrderingComparator());
      assertTrue(set.contains(list.toString()));
    }
  }
}
