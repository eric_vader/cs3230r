package com.e2empire.data.graph;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class GraphTest {

  @Parameters(name = "test({index})")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {0, 0}, {1, 1}, {2, 1}, {3, 2}, {4, 3}, {5, 5}, {6, 8}});
  }

  private int fInput;

  private int fExpected;

  public GraphTest(int input, int expected) {
    this.fInput = input;
    this.fExpected = expected;
  }

  @Test
  public void test() {
    assertTrue(true);
  }
}
