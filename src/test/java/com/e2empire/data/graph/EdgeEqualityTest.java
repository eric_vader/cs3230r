package com.e2empire.data.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Edge;
import com.e2empire.data.graph.Vertex;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collection;

/**
 * Tests the correct operation of the DirectedEdge class. Ensure that the equals function is
 * correctly implemented. This is to test the Equality for the DirectedTypes.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class EdgeEqualityTest {

  static Vertex<String> a = new Vertex<String>("1");
  static Vertex<String> b = new Vertex<String>("2");
  static Vertex<String> c = new Vertex<String>("3");
  static Vertex<String> d = new Vertex<String>("4");
  static Vertex<String> e = new Vertex<String>("5");
  static Vertex<String> f = new Vertex<String>("1");

  /**
   * @return data.
   */
  @Parameters(name = "Test[{index}] [{0}]==[{1}] is {2}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {new Edge(a, b), new Edge(a, b), true},
        {new Edge(a, b), new Edge(b, a), true}, {new Edge(a, b), new Object(), false},
        {new Edge(a, b), null, false}});
  }

  private Object input0;
  private Object input1;
  private boolean expected;
  private boolean hasFailed;

  /**
   * @param firstPair First Pair.
   * @param secondPair Second Pair.
   * @param isEquals <code>true</code> if the pair is equals, <code>false</code> otherwise.
   */
  public EdgeEqualityTest(Object firstPair, Object secondPair, boolean isEquals) {
    this.hasFailed = false;
    try {
      this.input0 = firstPair;
      this.input1 = secondPair;
    } catch (InvalidParameterException ipe) {
      this.hasFailed = true;
    }
    this.expected = isEquals;
  }

  /**
   * Tests the correct implementation of equals.
   */
  @Test
  public void test() {
    if (!this.hasFailed) {
      assertEquals(this.expected, this.input0.equals(this.input1));
    } else {
      assertEquals(this.expected, false);
    }
  }
}
