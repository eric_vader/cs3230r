package com.e2empire.data.graph;

import com.e2empire.data.graph.Graph.Vertices;

import java.util.ArrayList;
import java.util.List;


/**
 * A class that defines all the common Vertices.
 *
 * @author Han Liang Wee Eric(A0065517A)
 *
 */
public class Common {

  /**
   * Vertices for equality test.
   */
  static Vertex<String> a = new Vertex<String>("1");
  static Vertex<String> b = new Vertex<String>("2");
  static Vertex<String> c = new Vertex<String>("3");
  static Vertex<String> d = new Vertex<String>("4");
  static Vertex<String> e = new Vertex<String>("5");
  static Vertex<String> f = new Vertex<String>("1");

  static Edge pair0 = new Edge(a, b);
  static Edge pair1 = new Edge(a, b);
  static Edge pair2 = new Edge(a, b);
  static Edge pair3 = new Edge(a, b);

  /**
   * Vertices for testing of the Bron-Kerbosch algorithm.
   */
  static final Vertex<String> v1 = new Vertex<String>("V1");
  static final Vertex<String> v2 = new Vertex<String>("V2");
  static final Vertex<String> v3 = new Vertex<String>("V3");
  static final Vertex<String> v4 = new Vertex<String>("V4");
  static final Vertex<String> v5 = new Vertex<String>("V5");
  static final Vertex<String> v6 = new Vertex<String>("V6");
  static final Vertex<String> v7 = new Vertex<String>("V7");
  static final Vertex<String> v8 = new Vertex<String>("V8");
  static final Vertex<String> v9 = new Vertex<String>("V9");
  static final Vertex<String> v10 = new Vertex<String>("V10");

  /**
   * Builds the graph that is on the paper.
   *
   * @return The graph object.
   */
  public static final Graph getGraphOnPaper() {

    GraphBuilder gb = new GraphBuilder();
    // for vertex 1
    gb.addEdge(new Edge(v1, v2)).addEdge(new Edge(v1, v3)).addEdge(new Edge(v1, v4))
        .addEdge(new Edge(v1, v6));
    // for vertex 2
    gb.addEdge(new Edge(v2, v4)).addEdge(new Edge(v2, v3)).addEdge(new Edge(v2, v5));
    // for vertex 3
    gb.addEdge(new Edge(v3, v4)).addEdge(new Edge(v3, v5));
    // for vertex 4
    gb.addEdge(new Edge(v4, v5));
    // vertex 5 has nothing left to add
    // for vertex 6
    gb.addEdge(new Edge(v6, v7)).addEdge(new Edge(v6, v9)).addEdge(new Edge(v6, v10));
    // for vertex 7
    gb.addEdge(new Edge(v7, v9)).addEdge(new Edge(v7, v8));
    // for vertex 8
    gb.addEdge(new Edge(v8, v9));
    // for vertex 9
    gb.addEdge(new Edge(v9, v10));
    return gb.build();

  }

  /**
   * Build the maximal cliques for the paper.
   *
   * @return the maximal cliques for the paper.
   */
  public static final List<Vertices> getMaximalCliquesOnPaper() {
    /**
     * <pre>
     * c0 = 1,2,3,4
     * c1 = 2,3,4,5
     * c2 = 6,9,10
     * c3 = 6,7,9
     * c4 = 7,8,9
     * c5 = 1,6
     * </pre>
     */
    Graph graph = getGraphOnPaper();

    List<Vertices> maximalCliques = new ArrayList<>();
    maximalCliques.add(graph.getVertices(v1, v2, v3, v4));
    maximalCliques.add(graph.getVertices(v2, v3, v4, v5));
    maximalCliques.add(graph.getVertices(v6, v9, v10));
    maximalCliques.add(graph.getVertices(v6, v7, v9));
    maximalCliques.add(graph.getVertices(v7, v8, v9));
    maximalCliques.add(graph.getVertices(v1, v6));

    return maximalCliques;
  }

  /**
   * Builds the graph that is found on wikipedia.
   *
   * @return The graph object.
   */
  public static final Graph getGraphOnWiki() {

    GraphBuilder gb = new GraphBuilder();
    // v1
    gb.addEdge(new Edge(v1, v2)).addEdge(new Edge(v1, v5));
    // v2
    gb.addEdge(new Edge(v2, v3)).addEdge(new Edge(v2, v5));
    // v3
    gb.addEdge(new Edge(v3, v4));
    // v4
    gb.addEdge(new Edge(v4, v5)).addEdge(new Edge(v4, v6));

    return gb.build();
  }

  /**
   * Build the maximal cliques for graph found on wikipedia.
   *
   * @return The maximal cliques for the graph.
   */
  public static final List<Vertices> getMaximalCliquesOnWiki() {
    /**
     * <pre>
     * c0 = 4,6
     * c1 = 1,2,5
     * c2 = 2,3
     * c3 = 3,4
     * c4 = 4,5
     * </pre>
     */
    Graph graph = getGraphOnWiki();

    List<Vertices> maximalCliques = new ArrayList<Vertices>();
    maximalCliques.add(graph.getVertices(v4, v6));
    maximalCliques.add(graph.getVertices(v1, v2, v5));
    maximalCliques.add(graph.getVertices(v2, v3));
    maximalCliques.add(graph.getVertices(v3, v4));
    maximalCliques.add(graph.getVertices(v4, v5));
    return maximalCliques;
  }

  /**
   * @return The 3-Clique Communities based on the graph in the paper.
   */
  public static List<Vertices> get3CliqueCommunityOnPaper() {
    List<Vertices> communities = new ArrayList<>();
    Graph graph = getGraphOnPaper();
    communities.add(graph.getVertices(v6, v7, v8, v9, v10));
    communities.add(graph.getVertices(v1, v2, v3, v4, v5));
    return communities;
  }
}
