package com.e2empire.data.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Vertex;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests the correct operation of the Vertex class. Ensure that the hashcode function is properly
 * implemented.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class VertexHashcodeTest {

  /**
   * @return data.
   */
  @Parameters(name = "Test({index}) (is {1} in set) is {2}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {new int[] {1, 2, 3}, 1, true},
        {new int[] {1, 2, 3}, 0, false}, {new int[] {1}, 1, true}});
  }

  private Set<Vertex<Integer>> set;
  private Vertex<Integer> tryVertex;
  private boolean expected;

  /**
   * @param integerSet
   * @param tryInt
   * @param expected
   */
  public VertexHashcodeTest(int[] integerSet, int tryInt, boolean expected) {
    this.set = new HashSet<>();
    for (int eaInt : integerSet) {
      this.set.add(new Vertex<Integer>(eaInt));
    }
    this.tryVertex = new Vertex<Integer>(tryInt);
    this.expected = expected;
  }

  /**
   * Tests the correct implementation of equals.
   */
  @Test
  public void test() {
    assertEquals(this.set.contains(this.tryVertex), this.expected);
  }
}
