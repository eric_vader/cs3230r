package com.e2empire.data.graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Vertex;

import java.util.Arrays;
import java.util.Collection;

/**
 * Tests the correct operation of the Vertex class. Ensure that the equals function is correctly
 * implemented.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class VertexEqualityTest {

  /**
   * @return data.
   */
  @Parameters(name = "Test({index}) {0}=={1} is {2}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {new Vertex<String>("1"), new Vertex<String>("1"), true},
        {new Vertex<String>("2"), new Vertex<String>("1"), false},
        {new Vertex<String>("ABCD"), new Vertex<String>("ABCD"), true},
        {new Vertex<String>("1ae"), new Vertex<String>("1we"), false},
        {new Vertex<String>(""), null, false}, {new Vertex<String>(""), new Object(), false}});
  }

  private Object input0;
  private Object input1;
  private boolean expected;

  /**
   * @param input0
   * @param input1
   * @param expected
   */
  public VertexEqualityTest(Object input0, Object input1, boolean expected) {
    this.input0 = input0;
    this.input1 = input1;
    this.expected = expected;
  }

  /**
   * Tests the correct implementation of equals.
   */
  @Test
  public void test() {
    assertEquals(this.input0.equals(this.input1), this.expected);
  }
}
