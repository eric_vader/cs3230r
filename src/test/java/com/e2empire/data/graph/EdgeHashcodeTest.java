package com.e2empire.data.graph;

import static com.e2empire.data.graph.Common.pair0;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Edge;

/**
 * Tests the correct operation of the Edge class. Ensure that the hashcode function is properly
 * implemented.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class EdgeHashcodeTest {

  /**
   * @return data.
   */
  @Parameters(name = "Test({index}) (is {1} in set) is {2}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] {{new Edge[] {pair0}, pair0, true}});
  }

  private Set<Edge> set;
  private Edge directedEdgeSearch;
  private boolean isFound;

  /**
   * Test case.
   *
   * @param directedEdgeSet
   * @param directedEdgeSearch
   * @param isFound
   */
  public EdgeHashcodeTest(Edge[] directedEdgeSet, Edge directedEdgeSearch,
      boolean isFound) {
    this.set = new HashSet<>();
    for (Edge eaEdge : directedEdgeSet) {
      this.set.add(eaEdge);
    }
    this.directedEdgeSearch = directedEdgeSearch;
    this.isFound = isFound;
  }

  /**
   * Tests the correct implementation of equals.
   */
  @Test
  public void test() {
    assertEquals(this.set.contains(this.directedEdgeSearch), this.isFound);
  }
}
