package com.e2empire.data.graph;

import static com.e2empire.data.graph.Common.a;
import static com.e2empire.data.graph.Common.b;
import static com.e2empire.data.graph.Common.f;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.e2empire.data.graph.Edge;
import com.e2empire.data.graph.Vertex;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Collection;

/**
 * Tests the correct operation of the DirectedEdge class. Ensure that the equals function is
 * correctly implemented.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class EdgeEqualityTest2 {

  /**
   * @return data.
   */
  @Parameters(name = "Test[{index}] [{0}->{1}]==[{2}->{3}] is {4}")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {a, a, a, a, true}, {a, f, a, f, false},
        {a, b, a, b, true}, {a, b, b, a, true}, {null, b, null, b, false},
        {b, null, b, null, false}});
  }

  private Object input0;
  private Object input1;
  private boolean expected;
  private boolean hasFailed;

  /**
   * @param a0 First Pair, from.
   * @param a1 First pair, to.
   * @param b0 Second pair, from.
   * @param b1 Second pair, to.
   * @param isEquals <code>true</code> if the pair is equals, <code>false</code> otherwise.
   */
  public EdgeEqualityTest2(Vertex<String> a0, Vertex<String> a1, Vertex<String> b0,
      Vertex<String> b1, boolean isEquals) {
    this.hasFailed = false;
    try {
      this.input0 = new Edge(a0, a1);
      this.input1 = new Edge(b0, b1);
    } catch (InvalidParameterException ipe) {
      this.hasFailed = true;
    }
    this.expected = isEquals;
  }

  /**
   * Tests the correct implementation of equals.
   */
  @Test
  public void test() {
    if (!this.hasFailed) {
      assertEquals(this.expected, this.input0.equals(this.input1));
    } else {
      assertEquals(this.expected, false);
    }
  }
}
