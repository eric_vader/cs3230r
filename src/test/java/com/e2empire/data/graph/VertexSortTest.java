package com.e2empire.data.graph;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Tests the correct operation of the Vertex class. Ensure that the compareTo is correctly
 * implemented.
 *
 * @author Han Liang Wee, Eric
 */
@RunWith(Parameterized.class)
public class VertexSortTest {

  /**
   * @return data.
   */
  @Parameters(name = "Test Sort({index})")
  public static Collection<Object[]> data() {
    return Arrays.asList(new Object[][] { {new int[] {2, 1}, new int[] {1, 2}},
        {new int[] {1}, new int[] {1}}, {new int[] {2, 1, 3}, new int[] {1, 2, 3}},
        {new int[] {-2, 1, 999}, new int[] {-2, 1, 999}}});
  }

  private List<Vertex<Integer>> unsorted;
  private List<Vertex<Integer>> sorted;

  /**
   * Tests the sorting of the Vertex List.
   *
   * @param unsortedIntegerArray Unsorted integer array.
   * @param sortedIntegerArray Sorted integer array.
   */
  public VertexSortTest(int[] unsortedIntegerArray, int[] sortedIntegerArray) {
    this.unsorted = new ArrayList<>(unsortedIntegerArray.length);
    this.sorted = new ArrayList<>(sortedIntegerArray.length);
    for (int eaUnsorted : unsortedIntegerArray) {
      this.unsorted.add(new Vertex<Integer>(eaUnsorted));
    }
    for (int eaSorted : sortedIntegerArray) {
      this.sorted.add(new Vertex<Integer>(eaSorted));
    }
  }

  /**
   * Sorts and tests equality.
   */
  @Test
  public void test() {
    Collections.sort(this.unsorted, new Comparator<Vertex<Integer>>() {

      @Override
      public int compare(Vertex<Integer> o1, Vertex<Integer> o2) {
        return o1.getElement().compareTo(o2.getElement());
      }
    });
    assertTrue(this.sorted.equals(this.unsorted));
  }
}
