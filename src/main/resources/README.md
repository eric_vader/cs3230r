# ${project.name} #

Latest version: ${version}

Source code part of the CS3230R Project.

## Systems ##

### Technology ###

* [Git](http://git-scm.com/)
* [Maven](https://maven.apache.org/)
* [Junit](junit.orga)

### System requirements ###
```
    Linux 3.19.5-200 and above
    Apache Maven 3.2.2
    Git version 2.1.0
    Oracle Java HotSpot(TM) 64-Bit Server VM (build 25.40-b25, mixed mode)
```
Note that if you do not meet the requirements, your build can still work.

### How do I get set up? ###

1. git clone the repository. 
1. Build the project and run tests: `$ mvn build`.

## Developers ##

### Contribution guidelines ###

* Code must accopmany tests, we will *try* to achieve 100% path coverage.
* Codestyle should follow [Google Java Codestyle](https://google-styleguide.googlecode.com/svn/trunk/eclipse-java-google-style.xml)

### Need help? ###

Read the documentation!

### Contributors ###
* [Han Liang Wee Eric](https://bitbucket.org/eric_vader)
