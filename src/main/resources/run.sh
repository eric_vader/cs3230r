#!/bin/bash

if [ -z ${1} ] #note the lack of a $ sigil
then
    echo "Variable is unset"
    exit;
fi
for i in $( du --max-depth=1 $1 | sort -n | cut -f2 | sed 's/\.\/\?//' | rev | cut -f1 -d$'/' | rev | sed '$ d' ); do
	for x in `seq 0 2`; do
		echo "java -Xmx6G -Xms6G -XX:+UseParallelOldGC -XX:+UseNUMA -jar cs3230r-jar-with-dependencies.jar $1 $i $x 3 1"
		sleep 30
		java -Xmx6G -Xms6G -XX:+UseParallelOldGC -XX:+UseNUMA -jar cs3230r-jar-with-dependencies.jar $1 $i $x 3 1
		if [ $x == "2" ]; then
			for y in `seq 2 24`; do
				echo "java -Xmx6G -Xms6G -XX:+UseParallelOldGC -XX:+UseNUMA -jar cs3230r-jar-with-dependencies.jar $1 $i $x 3 $y"
				sleep 30
				java -Xmx6G -Xms6G -XX:+UseParallelOldGC -XX:+UseNUMA -jar cs3230r-jar-with-dependencies.jar $1 $i $x 3 $y
			done
		fi
	done
done
#java -jar cs3230r-jar-with-dependencies.jar /home/Eric_Vader/workspace/cs3230r/target/classes/graph/real cond-mat-2005 0 4 1
