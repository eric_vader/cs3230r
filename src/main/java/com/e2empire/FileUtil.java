package com.e2empire;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Useful File Utilities.
 *
 * @author Han Liang Wee, Eric
 */
public class FileUtil {

  private static final Logger logger = LoggerFactory.getLogger(FileUtil.class);

  /**
   * Takes in a file name and retrieves the file. The code will check through the classpath of the
   * application, it will also consider that the fileName can also be an absolute path.
   *
   * @param fileName Absolute path or relative path or just the name of the file in the classpath.
   * @return the file.
   * @throws FileNotFoundException if the file does not exist in the classpath or through its
   *         absolute path.
   * @throws MalformedURLException
   */
  public static final File getFileFromClassloader(String fileName) throws FileNotFoundException,
      MalformedURLException {

    URL url = getAbsolutePathOf(fileName);
    return new File(url.getFile());
  }

  /**
   * Takes in a file name and retrieves the file. The code will check through the classpath of the
   * application, it will also consider that the fileName can also be an absolute path.
   *
   * @param fileName Absolute path or relative path or just the name of the file in the classpath.
   * @return the file.
   * @throws FileNotFoundException if the file does not exist in the classpath or through its
   *         absolute path.
   * @throws MalformedURLException
   */
  public static final URL getAbsolutePathOf(String fileName) throws FileNotFoundException,
  MalformedURLException {

    logger.debug("Retriving file[{}] from class path.", fileName);

    ClassLoader loader = FileUtil.class.getClassLoader();

    // logger.debug(loader.getResource("/").toString());

    // it can be referenced from an absolute path
    File file = new File(fileName);

    // try to check in the classpath
    URL url = null;
    if (!file.isFile()) {
      url = loader.getResource(fileName);
    } else {
      return new URL(fileName);
    }

    if (url == null) {
      String errorMsg = String.format("File %s cannot be found.", fileName);
      logger.error(errorMsg);
      throw new FileNotFoundException(errorMsg);
    }

    logger.debug("Retriving file[{}] successful.", fileName);

    return url;

  }

  /**
   * Saves the file to disk, create the directories if it does not exist
   *
   * @param absolutePath Absolute path to the file.
   * @param fileInBytes The file in a byte array.
   * @throws IOException when there is some issue with creating the directories.
   */
  public static void saveToDisk(Path absolutePath, byte[] fileInBytes) throws IOException {
    logger.debug("Saving file[{}] to disk.", absolutePath);
    Files.createDirectories(absolutePath.getParent());
    Files.write(absolutePath, fileInBytes);
  }

  /**
   * Deletes a file from the disk.
   *
   * @param pathToFile path to delete from the file.
   * @throws IOException when there is an issue with the delete.
   */
  public static void deleteFromDisk(Path pathToFile) throws IOException {
    logger.debug("Deleting file[{}] from disk.", pathToFile);
    Files.delete(pathToFile);
  }

}
