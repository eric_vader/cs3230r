package com.e2empire;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.e2empire.algorithm.AlgorithmResult;
import com.e2empire.algorithm.graph.community.CliquePercolationMethod;
import com.e2empire.algorithm.graph.community.CliquePercolationMethodData;
import com.e2empire.algorithm.graph.community.CliquePercolationMethodParallelData;
import com.e2empire.algorithm.graph.community.OptimizedCliquePercolationMethod;
import com.e2empire.algorithm.graph.community.ParallelCommunityDetection;
import com.e2empire.algorithm.graph.maximalcliques.BronKerboschVertexOrdering;
import com.e2empire.data.graph.Edge;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;
import com.e2empire.data.graph.GraphBuilder;
import com.e2empire.data.graph.GraphTree;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.set.MergingUnionFind;
import com.e2empire.data.set.NonMergingUnionFind;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.util.io.gml.GMLReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Runs the application and generates the report.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class App {

  /**
   * GML Extension.
   */
  private static final String GML_EXT = "gml";
  private static final Logger logger = LoggerFactory.getLogger(App.class);

  /**
   * Some Test Application.
   *
   * @param args command line arguments from the user. There are 3 parameters. First is the GML File
   *        name. Next is the Mode, 0 for CPM, 1 for ParallelCPM. Lastly, id the k-community to be
   *        found.
   *
   * @throws IOException
   * @throws IllegalAccessException
   * @throws InstantiationException
   */
  public static void main(String[] args)
      throws IOException, InstantiationException, IllegalAccessException {
    try {
      String gmlBaseDir = args[0];
      String gmlFileName = args[1];
      int algoMode = Integer.parseInt(args[2]);
      int k = Integer.parseInt(args[3]);
      int nProcs = Integer.parseInt(args[4]);

      com.tinkerpop.blueprints.Graph gmlGraph = new TinkerGraph();
      InputStream in = new FileInputStream(
          new File(String.format("%s/%s/%s.%s", gmlBaseDir, gmlFileName, gmlFileName, GML_EXT))
              .getPath());
      GMLReader.inputGraph(gmlGraph, in);

      logger.debug("Graph:" + gmlGraph);

      // now we load the gml graph into our internal system
      GraphBuilder gb = new GraphBuilder();
      Map<com.tinkerpop.blueprints.Vertex, Vertex<Integer>> vertexLookup = new HashMap<>();
      for (com.tinkerpop.blueprints.Vertex eaGmlVertex : gmlGraph.getVertices()) {
        Vertex<Integer> internalVertex =
            new Vertex<Integer>(Integer.parseInt(eaGmlVertex.getId().toString()));
        vertexLookup.put(eaGmlVertex, internalVertex);
        gb.addVertex(internalVertex);
      }

      for (com.tinkerpop.blueprints.Edge eaEdge : gmlGraph.getEdges()) {
        Edge internalEdge = new Edge(vertexLookup.get(eaEdge.getVertex(Direction.IN)),
            vertexLookup.get(eaEdge.getVertex(Direction.OUT)));
        gb.addEdge(internalEdge);
      }

      Graph internalGraph = gb.build();

      int communitySize = 0;
      long executionTimeMsec = 0;
      long memoryUsed = 0;
      String algorithmName = "";

      if (algoMode == 0) {

        algorithmName = CliquePercolationMethod.class.getName();

        CliquePercolationMethod<MergingUnionFind<Vertex<?>, Vertices>> cpm =
            new CliquePercolationMethod(new BronKerboschVertexOrdering(), MergingUnionFind.class);

        CliquePercolationMethodData config = new CliquePercolationMethodData();
        config.setIntK(k);
        config.setGraph(internalGraph);

        AlgorithmResult<List<Vertices>> result = cpm.timedExecute(config);
        communitySize = result.getData().size();
        executionTimeMsec = result.getExecutionTime();
        memoryUsed = result.getMemoryInUse();

      } else if (algoMode == 1) {

        algorithmName = OptimizedCliquePercolationMethod.class.getName();

        OptimizedCliquePercolationMethod<MergingUnionFind<Vertex<?>, Vertices>> cpm =
            new OptimizedCliquePercolationMethod(new BronKerboschVertexOrdering(),
                MergingUnionFind.class);

        CliquePercolationMethodData config = new CliquePercolationMethodData();
        config.setIntK(k);
        config.setGraph(internalGraph);

        AlgorithmResult<List<Vertices>> result = cpm.timedExecute(config);
        communitySize = result.getData().size();
        executionTimeMsec = result.getExecutionTime();
        memoryUsed = result.getMemoryInUse();

      } else if (algoMode == 2) {

        algorithmName = ParallelCommunityDetection.class.getName();

        ParallelCommunityDetection<MergingUnionFind<Vertex<?>, GraphTree.Vertices>> cpm =
            new ParallelCommunityDetection(new BronKerboschVertexOrdering(),
                NonMergingUnionFind.class);

        CliquePercolationMethodParallelData config = new CliquePercolationMethodParallelData();
        config.setIntK(k);
        config.setGraph(internalGraph);
        config.setNumProcs(nProcs);

        AlgorithmResult<List<GraphTree.Vertices>> result = cpm.timedExecute(config);
        communitySize = result.getData().size();
        executionTimeMsec = result.getExecutionTime();
        memoryUsed = result.getMemoryInUse();

      }

      logger.debug(String.format(
          "Executed %d-Clique %s[%d threads] on the graph %s, Completed in %d ms, Using %d Bytes, With %d communities.",
          k, algorithmName, nProcs, gmlFileName, executionTimeMsec, memoryUsed, communitySize));
      logger.debug(String.format("%d,%s,%d,%s,%d,%d,%d", k, algorithmName, nProcs, gmlFileName,
          executionTimeMsec, memoryUsed, communitySize));
    } catch (Exception e) {
      logger.error("Exception Caught at Main", e);
    }
  }

}
