package com.e2empire.algorithm;

import java.util.HashMap;
import java.util.Map;

/**
 * A wrapper to classify the results of the current execution.
 *
 * @author Han Liang Wee, Eric
 * @param <R> The result of the execution, generated by the algorithm.
 */
public class AlgorithmResult<R> {

  private R data;
  private Map<Class<?>, AlgorithmResult<?>> subAlgorithmExecution;
  private long executionTime;
  private long memoryInUse;

  /**
   * Initializes a Algorithm result.
   */
  public AlgorithmResult() {
    this.subAlgorithmExecution = new HashMap<>();
  }

  /**
   * @return The data.
   */
  public R getData() {
    return this.data;
  }

  /**
   * @param data The data to set.
   */
  public void setData(R data) {
    this.data = data;
  }

  /**
   * @return The executionTime.
   */
  public long getExecutionTime() {
    return this.executionTime;
  }

  /**
   * @param executionTime The executionTime to set.
   */
  public void setExecutionTime(long executionTime) {
    this.executionTime = executionTime;
  }

  /**
   * @return the subAlgorithmExecution.
   */
  public Map<Class<?>, AlgorithmResult<?>> getSubAlgorithmExecution() {
    return this.subAlgorithmExecution;
  }

  /**
   * @param subAlgorithmExecution the subAlgorithmExecution to set.
   */
  public void setSubAlgorithmExecution(Map<Class<?>, AlgorithmResult<?>> subAlgorithmExecution) {
    this.subAlgorithmExecution = subAlgorithmExecution;
  }

  /**
   * @return the memoryInUse.
   */
  public long getMemoryInUse() {
    return this.memoryInUse;
  }

  /**
   * @param memoryInUse the memoryInUse to set.
   */
  public void setMemoryInUse(long memoryInUse) {
    this.memoryInUse = memoryInUse;
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "AlgorithmResult [data=" + this.data + ", subAlgorithmExecution="
        + this.subAlgorithmExecution + ", executionTime=" + this.executionTime + "]";
  }

}
