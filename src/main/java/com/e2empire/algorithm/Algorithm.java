package com.e2empire.algorithm;

import java.util.Date;

/**
 * Algorithm class that describes a generic algorithm.
 *
 * @author Han Liang Wee, Eric
 *
 * @param <D> Data that the Algorithm will take in to process.
 * @param <R> Results that the algorithm that will produce.
 */
public abstract class Algorithm<D, R> {

  /**
   * Executes the algorithm, along with it being timed.
   *
   * @param data The data that the algorithm needs to take in.
   * @return The result of the processing.
   * @throws IllegalAccessException when the constructor of the data structure(s) cannot be
   *         accessed.
   * @throws InstantiationException when the data structure(s) cannot be initialized.
   */
  public AlgorithmResult<R> timedExecute(D data)
      throws InstantiationException, IllegalAccessException {

    AlgorithmResult<R> result = new AlgorithmResult<R>();

    Date startDate = new Date();
    result.setData(this.execute(data));
    Date endDate = new Date();

    result.setExecutionTime(endDate.getTime() - startDate.getTime());

    // Get the Java runtime
    Runtime runtime = Runtime.getRuntime();
    // Run the garbage collector
    runtime.gc();
    // Calculate the used memory
    long memory = runtime.totalMemory() - runtime.freeMemory();
    result.setMemoryInUse(memory);

    return result;
  }

  /**
   * Executes the algorithm.
   *
   * @param data The data that the algorithm needs to take in.
   * @return The result of the processing.
   * @throws IllegalAccessException when the constructor of the data structure(s) cannot be
   *         accessed.
   * @throws InstantiationException when the data structure(s) cannot be initialized.
   */
  public abstract R execute(D data) throws InstantiationException, IllegalAccessException;

}
