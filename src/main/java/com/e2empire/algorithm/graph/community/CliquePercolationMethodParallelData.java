package com.e2empire.algorithm.graph.community;

/**
 * Defines additional data required by the parallel version of the algorithm.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class CliquePercolationMethodParallelData extends CliquePercolationMethodData {

  private int numProcs;

  /**
   * @return the numProcs.
   */
  public int getNumProcs() {
    return this.numProcs;
  }

  /**
   * @param numProcs the numProcs to set.
   */
  public void setNumProcs(int numProcs) {
    this.numProcs = numProcs;
  }

}
