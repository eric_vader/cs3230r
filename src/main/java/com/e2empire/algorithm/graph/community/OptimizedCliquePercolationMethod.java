package com.e2empire.algorithm.graph.community;

import com.e2empire.algorithm.Algorithm;
import com.e2empire.algorithm.graph.maximalcliques.MaximalCliquesAlgorithm;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.set.UnionFind;
import com.e2empire.data.set.UnionFindConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Clique Percolation Method.
 *
 * @author Han Liang Wee, Eric
 * @param <T> The Union-Find data structure that will be used together in the CPM algorithm.
 *
 */
public class OptimizedCliquePercolationMethod<T extends UnionFind<Vertex<?>, Vertices>>
    extends Algorithm<CliquePercolationMethodData, List<Vertices>> {

  private MaximalCliquesAlgorithm maximalCliquesAlgorithm;
  private Class<T> unionFindClass;

  /**
   * Creates a CPM algorithm that takes in an algorithm that finds a maximal clique algorithm.
   *
   * @param maximalCliquesAlgorithm the algorithm that will be used to find maximal cliques.
   * @param unionFindClass The Union Find class.
   */
  public OptimizedCliquePercolationMethod(MaximalCliquesAlgorithm maximalCliquesAlgorithm,
      Class<T> unionFindClass) {

    if (maximalCliquesAlgorithm == null) {
      throw new NullPointerException("maximalCliquesAlgorithm cannot be null.");
    }
    this.maximalCliquesAlgorithm = maximalCliquesAlgorithm;
    this.unionFindClass = unionFindClass;

  }

  /**
   * Retrieves the canonical elements P and Q of the sets containing p and q via 2 find operations.
   * If P!=Q then p and q are in two different sets which are merged with a union F.
   *
   * @param unionFindF The union-find F that will be used to merged the canonical vertex of p and q.
   * @param p One of the vertex. This is represented as a set, but actually it is just 1 vertex.
   * @param q The other vertex. This is represented as a set, but actually it is just 1 vertex.
   */
  private static <T extends UnionFind<Vertex<?>, Vertices>> void mergeSets(T unionFindF, Vertices p,
      Vertices q) {
    Vertices P = unionFindF.findCanonical(p);
    Vertices Q = unionFindF.findCanonical(q);
    if (!P.equals(Q)) {
      unionFindF.union(P, Q);
    }
  }

  @Override
  public List<Vertices> execute(CliquePercolationMethodData data)
      throws InstantiationException, IllegalAccessException {

    Graph graph = data.getGraph();
    int kUser = data.getIntK();

    List<Vertices> maximalCliques = this.maximalCliquesAlgorithm.execute(graph);

    int kMax = 0;
    for (Vertices eaMaxClique : maximalCliques) {
      int sizeOfMaxClique = eaMaxClique.size();
      if (kMax < sizeOfMaxClique) {
        kMax = sizeOfMaxClique;
      }
    }

    List<Vertices> returnThis = null;
    for (int kInt = 2; kInt <= kMax; kInt++) {
      int kMinus1Int = kInt - 1;
      // We only keep the maximal cliques that are more than or equals to k.
      // Those that fail this condition does not need to be computed.
      final int kFin = kInt;
      List<Vertices> kCliqueCommunity =
          maximalCliques.stream().filter(c -> c.size() >= kFin).collect(Collectors.toList());

      UnionFind<Vertex<?>, Vertices> unionFind = this.unionFindClass.newInstance();
      UnionFindConfig<Vertex<?>, Vertices> config = new UnionFindConfig<>();
      config.setListOfSets(new ArrayList<Vertices>(kCliqueCommunity));
      unionFind.initalize(config);

      for (int x = 0; x < kCliqueCommunity.size(); x++) {

        // ignore those that are done.
        for (int y = x + 1; y < kCliqueCommunity.size(); y++) {

          // we will only need to merge those cliques that are more than or equals to k-1
          Vertices commonVertices = kCliqueCommunity.get(x).intersect(kCliqueCommunity.get(y));
          if (commonVertices.size() >= (kMinus1Int)) {
            mergeSets(unionFind, kCliqueCommunity.get(x), kCliqueCommunity.get(y));
          }
        }
      }

      if (kInt == kUser) {
        returnThis = unionFind.getSets();
      } else {
        unionFind.getSets();
      }
    }
    return returnThis;
  }
}
