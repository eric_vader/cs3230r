package com.e2empire.algorithm.graph.community;

import com.e2empire.algorithm.Algorithm;
import com.e2empire.algorithm.AlgorithmResult;
import com.e2empire.algorithm.graph.maximalcliques.MaximalCliquesAlgorithm;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.GraphBuilder;
import com.e2empire.data.graph.GraphTree;
import com.e2empire.data.graph.GraphTree.Vertices;
import com.e2empire.data.graph.SizeAscendingComparator;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.set.UnionFind;
import com.e2empire.data.set.UnionFindConfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Defines the Parallel version of Clique Percolation Method.
 *
 * @author Han Liang Wee, Eric
 * @param <T> The class of Union Find that will be used.
 *
 */
public class ParallelCommunityDetection<T extends UnionFind<Vertex<?>, Vertices>>
    extends Algorithm<CliquePercolationMethodParallelData, List<Vertices>> {

  private MaximalCliquesAlgorithm maximalCliquesAlgorithm;
  private Class<T> unionFindClass;

  /**
   * Initializes the Parallel Community Detection.
   *
   * @param maximalCliquesAlgorithm The max clique algorithm that will be used to find the max
   *        clique.
   * @param unionFindClass The Union Find Class that will be used for UF in the current algorithm.
   */
  public ParallelCommunityDetection(MaximalCliquesAlgorithm maximalCliquesAlgorithm,
      Class<T> unionFindClass) {
    if (maximalCliquesAlgorithm == null) {
      throw new NullPointerException("maximalCliquesAlgorithm cannot be null.");
    }
    this.maximalCliquesAlgorithm = maximalCliquesAlgorithm;
    this.unionFindClass = unionFindClass;
  }

  /**
   * Retrieves the canonical elements P and Q of the sets containing p and q via 2 find operations.
   * If P!=Q then p and q are in two different sets which are merged with a union F.
   *
   * @param unionFindF The union-find F that will be used to merged the canonical vertex of p and q.
   * @param p One of the vertex. This is represented as a set, but actually it is just 1 vertex.
   * @param q The other vertex. This is represented as a set, but actually it is just 1 vertex.
   */
  private static <T extends UnionFind<Vertex<?>, GraphTree.Vertices>> void mergeSets(T unionFindF,
      GraphTree.Vertices p, GraphTree.Vertices q) {
    GraphTree.Vertices P = unionFindF.findCanonical(p);
    GraphTree.Vertices Q = unionFindF.findCanonical(q);
    if (!P.equals(Q)) {
      unionFindF.union(P, Q);
    }
  }

  /**
   * Connect me produces the connected components of the union of the two graphs without any
   * information on neither edges.
   *
   * @param unionFindF1 The Union-Find for which will be used in the mergeSet, which the merge will
   *        happen in.
   * @param unionFindF2 The Union-Find for which will be used to retrieve the canonical vertex.
   * @param verticesInF2 The list of vertices to cycle through to get the canonical vertices.
   */
  private static <T extends UnionFind<Vertex<?>, GraphTree.Vertices>> void connectMe(T unionFindF1,
      T unionFindF2) {
    for (GraphTree.Vertices u : unionFindF1.getOriginalSets()) {
      GraphTree.Vertices U = unionFindF2.findCanonical(u);
      if (!U.equals(u)) {
        mergeSets(unionFindF1, U, u);
      }
    }
  }

  @Override
  public List<Vertices> execute(CliquePercolationMethodParallelData data)
      throws InstantiationException, IllegalAccessException {

    AlgorithmResult<List<Graph.Vertices>> algorithmResult =
        this.maximalCliquesAlgorithm.timedExecute(data.getGraph());

    // convert to new tree
    GraphTree graphTree = new GraphTree(data.getGraph());
    List<Vertices> maximalCliques = new ArrayList<>();
    List<Graph.Vertices> maximalCliquesOld = algorithmResult.getData();
    for (Graph.Vertices eaVertices : maximalCliquesOld) {
      maximalCliques.add(graphTree.getVertices(eaVertices.toList()));
    }

    // create vertices for each maximal cliques
    GraphTree maxCliqueGraph = null;
    List<Vertex<Vertices>> vertexOfMaxClique = new ArrayList<>();
    ArrayList<Vertices> listOfVertices = new ArrayList<>();
    // k max is calculated as the size of the largest maximal clique
    int kMax = 0;
    { // initialize the graph and max cliques
      GraphBuilder gb = new GraphBuilder();
      int sizeOfMaxClique;
      int x = 0;
      for (Vertices eaMaxClique : maximalCliques) {
        sizeOfMaxClique = eaMaxClique.size();
        if (kMax < sizeOfMaxClique) {
          kMax = sizeOfMaxClique;
        }
        Vertex<Vertices> maxCliqueVertex = new Vertex<>(eaMaxClique);
        gb.addVertex(maxCliqueVertex);
        vertexOfMaxClique.add(maxCliqueVertex);
        x++;
      }

      Collections.sort(vertexOfMaxClique, new SizeAscendingComparator());

      maxCliqueGraph = gb.buildGraphTree();
      for (Vertex<Vertices> eaMaxCliqueVertex : vertexOfMaxClique) {
        listOfVertices.add(maxCliqueGraph.getVertices(eaMaxCliqueVertex));
      }
    }

    int p = data.getNumProcs();
    int l = maximalCliques.size();

    // create the threads as specified
    ExecutorService threadPool = Executors.newFixedThreadPool(p);

    // the task list
    Collection<Future<List<T>>> futures = new LinkedList<Future<List<T>>>();

    // for each processor, will be done in parallel
    final int kMaxFinal = kMax;
    for (int q = 0; q < p; q++) {

      CospocThread<T> cospocThread = new CospocThread<T>(p, q, l, kMaxFinal, graphTree,
          vertexOfMaxClique, listOfVertices, this.unionFindClass);

      futures.add(threadPool.submit(cospocThread));

    }

    // we merge the rest of the results into this one.
    List<T> masterResults = null;
    try {
      // code to wait for all the threads to finish.
      // we select the first one first
      Iterator<Future<List<T>>> ite = futures.iterator();
      // there must be at least 1 thread.
      Future<List<T>> thread0Future = ite.next();
      masterResults = thread0Future.get();

      while (ite.hasNext()) {

        // blocks until the thread is complete.
        List<T> toBeMerged = ite.next().get();
        for (int k = 2; k <= kMax; k++) {
          connectMe(masterResults.get(k), toBeMerged.get(k));
        }

      }
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }

    // shutdowns the threads
    threadPool.shutdown();

    List<Vertices> verticesList = new ArrayList<>();
    for (Vertices eaCliqueVertices : masterResults.get(data.getIntK()).getSets()) {
      Vertices emptySet = graphTree.getEmptyVertices();
      for (Vertex<?> eaCliqueVertex : eaCliqueVertices.toList()) {
        Vertices eaClique = (Vertices) eaCliqueVertex.getElement();
        emptySet = eaClique.union(emptySet);
      }
      verticesList.add(emptySet);
    }

    return verticesList;

  }

  /**
   * Each Thread for COSpoc, Each thread will run independently from each other and collate the
   * results.
   *
   * @author Han Liang Wee, Eric
   * @param <T> The Type of Union Find that this used.
   *
   */
  public static class CospocThread<T extends UnionFind<Vertex<?>, GraphTree.Vertices>>
      implements Callable<List<T>> {

    // the number of threads
    private int p;
    // the thread number
    private int q;
    // the number of max cliques
    private int l;
    // the maximum k can be achieved
    private int kMax;

    // max clique list of vertex, each vertex represent a max clique
    private List<Vertex<Vertices>> vertexOfMaxClique;

    // The UF that is used.
    private Class<T> unionFindClass;
    private ArrayList<Vertices> listOfVertices;
    private GraphTree originalGraph;

    /**
     * Creates a new COSpoc Thread.
     *
     * @param p number of threads.
     * @param q the thread number.
     * @param l number of max cliques.
     * @param kMax maximum k can be achieved.
     * @param maxCliqueGraph maximum clique graph.
     * @param vertexOfMaxClique List of vertex, each vertex represents a maximum clique.
     * @param listOfVertices The global list of vertices.
     * @param unionFindClass Union-Find class that will be used for UF.
     */
    public CospocThread(int p, int q, int l, int kMax, GraphTree originalGraph,
        List<Vertex<Vertices>> vertexOfMaxClique, ArrayList<Vertices> listOfVertices,
        Class<T> unionFindClass) {

      this.p = p;
      this.q = q;
      this.l = l;

      this.kMax = kMax;

      this.originalGraph = originalGraph;
      this.vertexOfMaxClique = vertexOfMaxClique;
      this.listOfVertices = listOfVertices;

      this.unionFindClass = unionFindClass;
    }

    @Override
    public List<T> call() throws Exception {

      // internal copy of the Array of Set.
      List<T> internalF = new ArrayList<>(this.kMax + 1);
      try {
        internalF.add(null);
        internalF.add(null);
        for (int k = 2; k <= this.kMax; k++) {
          internalF.add(k, this.unionFindClass.newInstance());
          UnionFindConfig<Vertex<?>, GraphTree.Vertices> initialData = new UnionFindConfig<>();
          List<Vertices> filteredVertices = new ArrayList<>();

          // List<Vertex<Graph.Vertices>> vertexOfMaxClique,
          Vertices verticesSize = this.originalGraph.new Vertices(k);
          int index = Collections.binarySearch(this.vertexOfMaxClique,
              new Vertex<Vertices>(verticesSize), new SizeAscendingComparator());
          // because it finds the last element, so when we need to include all that are >=k
          if (index < 0) {
            index = index * -1 - 1;
          } else {
            while ((index >= 1)
                && (this.vertexOfMaxClique.get(index - 1).getElement().size() >= k)) {
              index--;
            }
          }
          filteredVertices = this.listOfVertices.subList(index, this.listOfVertices.size());

          initialData.setListOfSets(filteredVertices);
          internalF.get(k).initalize(initialData);
        }

        // initialize collections of disjoint sets
        for (int i = 0; i < this.l; i++) {
          if ((i % this.p) == this.q) {

            for (int j = i + 1; j < this.l; j++) {

              int numOverlap = this.vertexOfMaxClique.get(i).getElement()
                  .intersect(this.vertexOfMaxClique.get(j).getElement()).size();
              for (int k = 2; k <= (numOverlap + 1); k++) {
                ParallelCommunityDetection.mergeSets(internalF.get(k), this.listOfVertices.get(i),
                    this.listOfVertices.get(j));
              }

            }

          }
        }

      } catch (Exception e) {
        e.printStackTrace();
      }
      return internalF;
    }

  }

}
