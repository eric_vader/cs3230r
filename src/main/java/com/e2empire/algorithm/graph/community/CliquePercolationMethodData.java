package com.e2empire.algorithm.graph.community;

import com.e2empire.data.graph.Graph;

/**
 * The data needed by the CPM.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class CliquePercolationMethodData {

  private Graph graph;
  private int intK;

  /**
   * Retrieve the graph.
   *
   * @return the graph.
   */
  public Graph getGraph() {
    return this.graph;
  }

  /**
   * Sets graph.
   *
   * @param graph the graph to set.
   */
  public void setGraph(Graph graph) {
    this.graph = graph;
  }

  /**
   * Retrieve the intK.
   *
   * @return the intK.
   */
  public int getIntK() {
    return this.intK;
  }

  /**
   * Sets intK.
   *
   * @param intK the intK to set.
   */
  public void setIntK(int intK) {
    this.intK = intK;
  }

}
