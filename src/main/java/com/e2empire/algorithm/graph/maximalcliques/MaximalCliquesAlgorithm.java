package com.e2empire.algorithm.graph.maximalcliques;

import com.e2empire.algorithm.Algorithm;
import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.List;

/**
 * An algorithm that retrieves the maximal cliques.
 *
 * @author Han Liang Wee, Eric
 *
 */
public abstract class MaximalCliquesAlgorithm extends Algorithm<Graph, List<Vertices>> {

  @Override
  public abstract List<Vertices> execute(Graph data);

}
