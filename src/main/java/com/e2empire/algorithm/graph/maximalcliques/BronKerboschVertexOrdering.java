package com.e2empire.algorithm.graph.maximalcliques;

import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.graph.VertexComparator;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Implements the Bron-Kerbosch algorithm without pivioting
 *
 * @author Han Liang Wee, Eric
 */
public class BronKerboschVertexOrdering extends BronKerboschWithPivot {

  @Override
  public List<Vertices> execute(Graph graph) {

    List<Vertices> result = new ArrayList<Vertices>();

    Vertices setP = graph.getAllVertices();
    Vertices setR = graph.getEmptyVertices();
    Vertices setX = graph.getEmptyVertices();

    List<Vertex<?>> pivotList = graph.getAllVertices().toList();
    Collections.sort(pivotList, new VertexComparator(graph));

    for (Vertex<?> eaVertex : pivotList) {
      Vertices setWithEaVertex = graph.getVertices(eaVertex);
      Vertices neighboursOfEaVertex = graph.getNeighbourOf(eaVertex);
      // call the version 2 to execute
      this.execute(setR.union(setWithEaVertex), setP.intersect(neighboursOfEaVertex),
          setX.intersect(neighboursOfEaVertex), graph, result);
      setP = setP.complement(setWithEaVertex);
      setX = setX.union(setWithEaVertex);
    }

    return result;

  }

}
