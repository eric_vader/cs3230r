package com.e2empire.algorithm.graph.maximalcliques;

import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.ArrayList;
import java.util.List;

/**
 * Bron-Kerbosch algorithm that finds the maximal cliques in an undirected graph. It does not assume
 * a particular strategy to get the vertices to loop over.
 *
 * @author Han Liang Wee, Eric
 *
 */
public abstract class BronKerbosch extends MaximalCliquesAlgorithm {

  @Override
  public List<Vertices> execute(Graph graph) {

    Vertices setR = graph.getEmptyVertices();
    Vertices setP = graph.getAllVertices();
    Vertices setX = graph.getEmptyVertices();

    List<Vertices> maximalCliques = new ArrayList<Vertices>();

    this.execute(setR, setP, setX, graph, maximalCliques);

    return maximalCliques;

  }

  /**
   * Executes the Bron-Kerbosch algorithm with 3 parameters needed to execute the Bron-Kerbosch
   * algorithm. This function does not assume a particular strategy to select the vertices to
   * continue the recursion.
   *
   * <pre>
   *    BronKerbosch1(R, P, X):
   *        if P and X are both empty:
   *            report R as a maximal clique
   *        for each vertex v in getVertexSet():
   *            BronKerbosch1(R ⋃ {v}, P ⋂ N(v), X ⋂ N(v))
   *            P := P \ {v}
   *            X := X ⋃ {v}
   * </pre>
   *
   * @param setR The step by step build up of a solution of the maximum clique.
   * @param setP Potential set of vertices to add to the setR, which are all the common neighbors in
   *        setR.
   * @param setX The set that is traversed before on previous iterations, to prevent the algorithm
   *        from finding duplicate maximal cliques.
   *
   */
  void execute(Vertices setR, Vertices setP, Vertices setX, Graph graph, List<Vertices> result) {

    if (setP.isEmpty() && setX.isEmpty()) {
      result.add(setR);
      return;
    }

    for (Vertex<?> eaVertex : this.getVertexSet(setR, setP, setX, graph)) {
      Vertices setWithEaVertex = graph.getVertices(eaVertex);
      Vertices neighboursOfEaVertex = graph.getNeighbourOf(eaVertex);
      this.execute(setR.union(setWithEaVertex), setP.intersect(neighboursOfEaVertex),
          setX.intersect(neighboursOfEaVertex), graph, result);
      setP = setP.complement(setWithEaVertex);
      setX = setX.union(setWithEaVertex);
    }

  }

  /**
   * Defines the vertices selection strategy that the Bron-Kerbosch uses.
   *
   * @param setR The step by step build up of a solution of the maximum clique.
   * @param setP Potential set of vertices to add to the setR, which are all the common neighbors in
   *        setR.
   * @param setX The set that is traversed before on previous iterations, to prevent the algorithm
   *        from finding duplicate maximal cliques.
   * @param graph The graph that the algorithm is running on.
   * @return The list of vertices that will be used in iteration.
   */
  abstract Vertices getVertexSet(Vertices setR, Vertices setP, Vertices setX, Graph graph);

}
