package com.e2empire.algorithm.graph.maximalcliques;

import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Graph.Vertices;


/**
 * Implements the Bron-Kerbosch algorithm without pivioting.
 *
 * @author Han Liang Wee, Eric
 */
public class BronKerboschWithoutPivot extends BronKerbosch {

  @Override
  Vertices getVertexSet(Vertices setR, Vertices setP, Vertices setX, Graph graph) {
    return setP;
  }

}
