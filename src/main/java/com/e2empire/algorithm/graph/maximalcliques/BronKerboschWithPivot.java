package com.e2empire.algorithm.graph.maximalcliques;

import com.e2empire.data.graph.Graph;
import com.e2empire.data.graph.Vertex;
import com.e2empire.data.graph.VertexComparator;
import com.e2empire.data.graph.Graph.Vertices;

import java.util.Collections;
import java.util.List;


/**
 * Implements the Bron-Kerbosch algorithm without pivioting.
 *
 * @author Han Liang Wee, Eric
 */
public class BronKerboschWithPivot extends BronKerbosch {

  @Override
  Vertices getVertexSet(Vertices setR, Vertices setP, Vertices setX, Graph graph) {

    Vertices pivotSet = setP.union(setX);
    List<Vertex<?>> pivotList = pivotSet.toList();
    Collections.sort(pivotList, new VertexComparator(graph));

    Vertex<?> vertexU = pivotList.get(pivotList.size() - 1);

    return setP.complement(graph.getNeighbourOf(vertexU));
  }
}
