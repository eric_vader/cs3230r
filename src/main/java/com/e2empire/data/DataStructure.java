package com.e2empire.data;

/**
 * Defines a generic data structure.
 *
 * @author Han Liang Wee, Eric
 *
 * @param <I> The initial set of data that is needed to initialize the data structure.
 */
public abstract class DataStructure<I> {

  private boolean isInitialized;
  private I initialData;

  /**
   * Creates a data structure with isInitialized to be false.
   */
  public DataStructure() {
    this.isInitialized = false;
  }

  /**
   * Initializes the data structure. Each data structure can only be initialized once.
   *
   * @param initialData The initial data that is needed by this data structure.
   * @throws IllegalStateException if the data structure is initialized twice.
   */
  public void initalize(I initialData) throws IllegalStateException {

    this.ensureNotInitialized();

    this.initialData = initialData;
    this.isInitialized = true;

  }

  /**
   * Check if the data structure has been initialized.
   *
   * @throws IllegalStateException if the data structure was not initialized.
   */
  public final void ensureInitialized() throws IllegalStateException {

    if (!this.isInitialized) {
      throw new IllegalStateException("Data structure was not initialized.");
    }

  }

  /**
   * Check if the data structure has not been initialized.
   *
   * @throws IllegalStateException if the data structure was initialized.
   */
  public final void ensureNotInitialized() throws IllegalStateException {

    if (this.isInitialized) {
      throw new IllegalStateException("Data structure was initialized.");
    }

  }

  /**
   * @return the isInitialized.
   */
  public boolean isInitialized() {
    return this.isInitialized;
  }

  /**
   * @return the initialData.
   */
  public I getInitialData() {
    return this.initialData;
  }

}
