package com.e2empire.data.set;

import com.e2empire.data.DataStructure;
import com.e2empire.data.graph.Set;

import java.util.List;


/**
 * Union-Find data structure to support Union-Find functionality.
 *
 * @author Han Liang Wee, Eric
 * @param <E> The data that is backing the set.
 * @param <T> The set that is backing the UF data structure.
 *
 */
public abstract class UnionFind<E, T extends Set<E, T>>
    extends DataStructure<UnionFindConfig<E, T>> {

  /**
   * Unions the 2 sets v0 and v1 together.
   *
   * @param v0 The first set.
   * @param v1 The second set.
   */
  public abstract void union(T v0, T v1);

  /**
   * Finds the merged set.
   *
   * @param orignalSet the original set.
   * @return the merged set.
   */
  public abstract T findMerged(T orignalSet);

  /**
   * Finds the canonical set. A Canonical element is an arbitrary but unique element identified
   * within each set, which is used to represent the set.
   *
   * @param orignalSet the original set.
   * @return the merged set.
   */
  public abstract T findCanonical(T orignalSet);

  /**
   * Returns the sets that are left after all the merging operations.
   *
   * @return the sets.
   */
  public abstract List<T> getSets();

  /**
   * Returns the sets that are from the original merging operations.
   *
   * @return the sets.
   */
  public abstract List<T> getOriginalSets();

}
