package com.e2empire.data.set;

import com.e2empire.data.graph.Set;

import java.util.List;

/**
 * Defines a class that details the data needed to initialize the Union-Find data structure.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class UnionFindConfig<E, T extends Set<E, T>> {

  private List<T> listOfSets;

  /**
   * @return the listOfSets.
   */
  public List<T> getListOfSets() {
    return this.listOfSets;
  }

  /**
   * @param listOfSets the listOfSets to set.
   */
  public void setListOfSets(List<T> listOfSets) {
    this.listOfSets = listOfSets;
  }

}
