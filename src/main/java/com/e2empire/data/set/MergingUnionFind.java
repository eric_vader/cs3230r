package com.e2empire.data.set;

import com.e2empire.data.graph.Set;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import edu.princeton.cs.algorithms.UF;

/**
 * Simple implementation of the Union Find data structure.
 *
 * @author Han Liang Wee, Eric
 *
 * @param <E> The data.
 * @param <T> The set.
 */
public class MergingUnionFind<E, T extends Set<E, T>> extends UnionFind<E, T> {

  /**
   * original data.
   */
  private Map<T, Integer> indexLookup;
  /**
   * Merged data of vertices and etc...
   */
  private ArrayList<T> mergedArray;
  private ArrayList<T> originalArray;
  private UF backingUF;

  @Override
  public void initalize(UnionFindConfig<E, T> initialData) throws IllegalStateException {
    super.initalize(initialData);
    this.mergedArray = new ArrayList<>(this.getInitialData().getListOfSets());
    this.originalArray = new ArrayList<>(this.getInitialData().getListOfSets());
    this.indexLookup = new HashMap<>();
    int index = 0;
    for (T eaSet : this.mergedArray) {
      this.indexLookup.put(eaSet, index);
      index++;
    }
    this.backingUF = new UF(index);
  }

  @Override
  public void union(T v0, T v1) {
    int originalIndexV0 = this.indexLookup.get(v0);
    int originalIndexV1 = this.indexLookup.get(v1);
    this.backingUF.union(originalIndexV0, originalIndexV1);
    int rootIndex = this.backingUF.find(originalIndexV0);
    T rootSet = this.mergedArray.get(rootIndex);
    // update the set with the union.
    rootSet = v0.union(v1).union(rootSet);
    this.mergedArray.set(originalIndexV0, null);
    this.mergedArray.set(originalIndexV1, null);
    this.mergedArray.set(rootIndex, rootSet);
  }

  @Override
  public T findMerged(T orignalSet) {
    int originalIndex = this.indexLookup.get(orignalSet);
    return this.mergedArray.get(this.backingUF.find(originalIndex));
  }

  @Override
  public List<T> getSets() {
    return this.mergedArray.stream().filter((ea) -> (ea != null)).collect(Collectors.toList());
  }

  @Override
  public T findCanonical(T orignalSet) {
    if (this.indexLookup.get(orignalSet) == null) {
      return null;
    }
    int originalIndex = this.indexLookup.get(orignalSet);
    return this.originalArray.get(this.backingUF.find(originalIndex));
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "SimpleUnionFind [indexLookup=" + this.indexLookup + ", mergedArray=" + this.mergedArray
        + ", originalArray=" + this.originalArray + ", backingUF=" + this.backingUF + "]";
  }

  @Override
  public List<T> getOriginalSets() {
    return this.originalArray;
  }

}
