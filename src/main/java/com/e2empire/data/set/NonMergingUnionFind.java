package com.e2empire.data.set;

import org.apache.commons.lang.NotImplementedException;

import com.e2empire.data.graph.Set;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import edu.princeton.cs.algorithms.UF;

/**
 * Implementation of the Union Find data structure that will not perform the merge during union.
 *
 * @author Han Liang Wee, Eric
 *
 * @param <E> The data.
 * @param <T> The set.
 */
public class NonMergingUnionFind<E, T extends Set<E, T>> extends UnionFind<E, T> {

  /**
   * original data.
   */
  private Map<T, Integer> indexLookup;
  private ArrayList<T> originalArray;
  private UF backingUF;

  @Override
  public void initalize(UnionFindConfig<E, T> initialData) throws IllegalStateException {
    super.initalize(initialData);
    this.originalArray = new ArrayList<>(this.getInitialData().getListOfSets());
    this.indexLookup = new HashMap<>();
    int index = 0;
    for (T eaSet : this.originalArray) {
      this.indexLookup.put(eaSet, index);
      index++;
    }
    this.backingUF = new UF(index);
  }

  @Override
  public void union(T v0, T v1) {
    int originalIndexV0 = this.indexLookup.get(v0);
    int originalIndexV1 = this.indexLookup.get(v1);
    this.backingUF.union(originalIndexV0, originalIndexV1);
  }

  @Override
  public T findMerged(T orignalSet) {
    throw new NotImplementedException("findMerged not supported.");
  }

  @Override
  public List<T> getSets() {

    ArrayList<T> mergedSets = new ArrayList<>(this.originalArray);

    for (int x = 0; x < mergedSets.size(); x++) {

      int rootIndex = this.backingUF.find(x);
      if (rootIndex != x) {
        mergedSets.set(rootIndex, mergedSets.get(rootIndex).union(this.originalArray.get(x)));
        mergedSets.set(x, null);
      }

    }

    return mergedSets.stream().filter((ea) -> ea != null).collect(Collectors.toList());

  }

  @Override
  public T findCanonical(T orignalSet) {
    if (this.indexLookup.get(orignalSet) == null) {
      return null;
    }
    int originalIndex = this.indexLookup.get(orignalSet);
    return this.originalArray.get(this.backingUF.find(originalIndex));
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "NonMergingUnionFind [indexLookup=" + this.indexLookup + ", originalArray="
        + this.originalArray + ", backingUF=" + this.backingUF + "]";
  }

  @Override
  public List<T> getOriginalSets() {
    return this.originalArray;
  }

}
