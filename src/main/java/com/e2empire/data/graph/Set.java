package com.e2empire.data.graph;

import java.util.List;

/**
 * Defines a Set. The class should be immutable.
 *
 * @author Han Liang Wee, Eric
 * @param <E> The Element in the set.
 * @param <T> The Subclass that will inherit Set, to provide the functionailtiy behind the set
 *        operations.
 */
public interface Set<E, T extends Set<E, T>> extends Iterable<E> {

  /**
   * Take 2 sets and union them together into another set. The operation is commutative.
   *
   * @param toUnionWith The set to union with.
   * @return A new set that will be the union of the 2 sets.
   */
  public T union(T toUnionWith);

  /**
   * Take 2 sets and intersect them together into another set. This operation is commutative.
   *
   * @param toIntersectWith The set of to intersect with.
   * @return A new set that will be the intersection of the 2 sets.
   */
  public T intersect(T toIntersectWith);

  /**
   * Take 2 sets and subtract the set toSubtract from the this vertices set.
   *
   * @param toSubtract The set of vertices to subtract with.
   * @return A new set that will be the result of the subtraction.
   */
  public T complement(T toSubtract);

  /**
   * Take 2 sets and perform this * toMultiplyWith and return the result. This operation is
   * commutative.
   *
   * @param toMultiplyWith The set to be multiplied with.
   * @return A new set that will be the result of the cartesian product.
   */
  public T cartesianProduct(T toMultiplyWith);

  /**
   * Checks if the set is empty.
   *
   * @return <code>true</code> if empty, <code>false</code> otherwise.
   */
  public boolean isEmpty();

  /**
   * Converts the Set to a List.
   *
   * @return the list that is behind the set.
   */
  public List<E> toList();

  /**
   * Returns the number of elements in the set.
   *
   * @return the size of the set.
   */
  public int size();

}
