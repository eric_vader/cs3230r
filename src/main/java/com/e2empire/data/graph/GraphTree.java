package com.e2empire.data.graph;

import org.apache.commons.collections4.map.MultiValueMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Defines a Graph with Vertices and Edges, Vertices implemented using trees instead of the normal
 * bitmap.
 *
 * @author Han Liang Wee, Eric
 */
public class GraphTree {

  private Vertex<?>[] backingArray;
  // lookup the list of vertices connected to the key of the lookup.
  private MultiValueMap<Vertex<?>, Vertex<?>> lookupNeighbours;
  // retrieves the index location of the vertex in the backing array.
  private Map<Vertex<?>, Integer> reverseIndexLookup;
  private Edge[] backingEdges;

  /**
   * Creates a graph with the following edges, the vertices will be added automatically from the set
   * of edges.
   *
   * @param edgesArray Edges that are present in the graph.
   * @param vertexArray Array of vertices.
   */
  GraphTree(Vertex<?>[] vertexArray, Edge[] edgesArray) {

    this.backingArray = vertexArray;
    this.backingEdges = edgesArray;
    HashSet<Vertex<?>> validVerticesSet = new HashSet<Vertex<?>>();
    this.reverseIndexLookup = new HashMap<Vertex<?>, Integer>();

    int temp = 0;
    for (Vertex<?> eaVertex : this.backingArray) {
      validVerticesSet.add(eaVertex);
      this.reverseIndexLookup.put(eaVertex, temp);
      temp++;
    }
    if (validVerticesSet.size() != vertexArray.length) {
      throw new IllegalStateException("Vertices in the vertex array must be unique.");
    }

    // in this lookup, if the edges are not connected to anything they will be null.
    this.lookupNeighbours = new MultiValueMap<>();

    // this set is to remember traversed nodes
    for (Edge eaEdge : edgesArray) {

      if (!validVerticesSet.contains(eaEdge.getV0())) {
        throw new IllegalStateException(
            "Must initialize the vertex Array with all the possible vertices in the graph.");
      }

      if (!validVerticesSet.contains(eaEdge.getV1())) {
        throw new IllegalStateException(
            "Must initialize the vertex Array with all the possible vertices in the graph.");
      }

      if (!eaEdge.getV0().equals(eaEdge.getV1())) {
        this.lookupNeighbours.put(eaEdge.getV0(), eaEdge.getV1());
      }
      this.lookupNeighbours.put(eaEdge.getV1(), eaEdge.getV0());

    }
  }

  /**
   * Converts a graph to a tree graph.
   *
   * @param graph
   */
  public GraphTree(Graph graph) {
    this(graph.getBackingArray(), graph.getBackingEdges());
  }

  /**
   * Get an empty set of vertices.
   *
   * @return the Empty set of vertices.
   */
  public Vertices getEmptyVertices() {
    return new Vertices();
  }

  /**
   * Retrieves the set that contains the vertex specified.
   *
   * @param vertexArray The array of vertex that will be used to create the vertices.
   * @return the Vertices that contain all the vertices in the vertexArray.
   */
  public Vertices getVertices(Vertex<?>... vertexArray) {

    if ((vertexArray == null) || (vertexArray.length == 0)) {
      return new Vertices();
    }

    SET<Vertex<?>> set = new SET<Vertex<?>>();
    for (Vertex<?> eaVertex : vertexArray) {
      set.add(eaVertex);
    }

    return new Vertices(set);
  }

  /**
   * Retrieves the set that contains the vertex specified.
   *
   * @param vertexArray The array of vertex that will be used to create the vertices.
   * @return the Vertices that contain all the vertices in the vertexArray.
   */
  public Vertices getVertices(List<Vertex<?>> vertexArray) {

    if ((vertexArray == null) || (vertexArray.size() == 0)) {
      return new Vertices();
    }

    SET<Vertex<?>> set = new SET<Vertex<?>>();
    for (Vertex<?> eaVertex : vertexArray) {
      set.add(eaVertex);
    }

    return new Vertices(set);
  }

  /**
   * Retrieves the neighbor of the specified vertex.
   *
   * @param vertex The vertex for which the neighbors will be retrieved.
   * @return The vertices that are neighbors of the vertex.
   */
  public Vertices getNeighbourOf(Vertex<?> vertex) {
    Collection<Vertex<?>> vertexCollection = this.lookupNeighbours.getCollection(vertex);
    if (vertexCollection != null) {
      return this.getVertices(vertexCollection.toArray(new Vertex<?>[vertexCollection.size()]));
    } else {
      return new Vertices();
    }
  }

  /**
   * Defines the vertices in a graph, defined not as a separate class because it can rely on some
   * properties of the current graph. Note that the set of vertices is immutable.
   *
   * @author Han Liang Wee, Eric
   */
  public class Vertices
      implements com.e2empire.data.graph.Set<Vertex<?>, Vertices>, Comparable<Vertices> {

    private SET<Vertex<?>> set;
    private int size;

    /**
     * Creates an non empty Vertices set.
     *
     * @param resultBitmap the backing bitmap to back the vertices object.
     */
    private Vertices(SET<Vertex<?>> set) {
      this.set = set;
    }

    /**
     * Creates a fake Vertices, just to use for comparator.
     *
     * @param size size to set.
     */
    public Vertices(int size) {
      this.size = size;
    }

    /**
     * Creates an empty Vertices set.
     */
    public Vertices() {
      this.set = new SET<Vertex<?>>();
    }

    @Override
    public Vertices union(Vertices toUnionWith) {
      return new Vertices(this.set.union(toUnionWith.set));

    }

    @Override
    public Vertices intersect(Vertices toIntersectWith) {
      return new Vertices(this.set.intersects(toIntersectWith.set));
    }

    @Override
    public Vertices complement(Vertices toSubtract) {
      throw new UnsupportedOperationException(
          "This operation does not apply for the set of vertices.");
    }

    @Override
    public Vertices cartesianProduct(Vertices toMultiplyWith) {
      throw new UnsupportedOperationException(
          "This operation does not apply for the set of vertices.");
    }

    @Override
    public boolean isEmpty() {
      return this.set.isEmpty();
    }

    @Override
    public Iterator<Vertex<?>> iterator() {
      return this.set.iterator();
    }

    @Override
    public List<Vertex<?>> toList() {
      Iterator<Vertex<?>> iter = this.iterator();
      List<Vertex<?>> copy = new ArrayList<>();
      while (iter.hasNext()) {
        copy.add(iter.next());
      }
      return copy;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
      return this.toList().toString();
    }

    @Override
    public int size() {
      if (this.set == null) {
        return this.size;
      } else {
        return this.set.size();
      }
    }

    private GraphTree getOuterType() {
      return GraphTree.this;
    }

    @Override
    public int compareTo(Vertices other) {
      return Integer.compare(this.hashCode(), other.hashCode());
    }
  }


  /**
   * Defines the Edges in a graph, similarly, defined here as there is some contextual meaning.
   *
   * @author Han Liang Wee, Eric
   */
  public class Edges {

  }

  /**
   * Retrieves all the vertices in the graph.
   *
   * @return the Vertices that contain all the vertices in the graph.
   */
  public Vertices getAllVertices() {
    return this.getVertices(this.backingArray);
  }

  /**
   * Returns the number of edges for the mentioned vertex.
   *
   * @param vertex the vertex to retrieve the number of edges.
   * @return the number of edges that concerns the vertex.
   */
  public int getNumberOfEdges(Vertex<?> vertex) {
    Collection<Vertex<?>> neighbours = this.lookupNeighbours.getCollection(vertex);
    if (neighbours != null) {
      return neighbours.size();
    } else {
      return 0;
    }
  }
}
