package com.e2empire.data.graph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Defines a graph builder. This is a tool to build a graph.
 *
 * @author Han Liang Wee, Eric
 */
public class GraphBuilder {

  private HashSet<Vertex<?>> vertexList;
  private List<Edge> edgesList;

  /**
   * Initializes a new graph builder.
   */
  public GraphBuilder() {
    this.vertexList = new HashSet<Vertex<?>>();
    this.edgesList = new ArrayList<Edge>();
  }

  /**
   * Builds the Graph.
   *
   * @return the graph that is built.
   */
  public Graph build() {
    Vertex<?>[] vertexArray = this.vertexList.toArray(new Vertex[this.vertexList.size()]);
    Edge[] edgeArray = this.edgesList.toArray(new Edge[this.edgesList.size()]);
    return new Graph(vertexArray, edgeArray);
  }

  /**
   * @return The Graph Tree.
   */
  public GraphTree buildGraphTree() {
    Vertex<?>[] vertexArray = this.vertexList.toArray(new Vertex[this.vertexList.size()]);
    Edge[] edgeArray = this.edgesList.toArray(new Edge[this.edgesList.size()]);
    return new GraphTree(vertexArray, edgeArray);
  }

  /**
   * Adds vertices to the current graph.
   *
   * @param vertexArray the vertices that will be added to the current graph.
   *
   * @return the graph builder.
   */
  public GraphBuilder addVertex(Vertex<?>... vertexArray) {

    if (vertexArray == null) {
      throw new NullPointerException("vertexArray cannot be null.");
    }
    for (Vertex<?> eaVertex : vertexArray) {
      this.vertexList.add(eaVertex);
    }
    return this;

  }

  /**
   * Adds edges to the current graph.
   *
   * @param edgeArray the edges that will be added to the current graph.
   * @return the graph builder.
   */
  public GraphBuilder addEdge(Edge... edgeArray) {
    if (edgeArray == null) {
      throw new NullPointerException("edgeArray cannot be null.");
    }
    for (Edge eaEdge : edgeArray) {
      this.edgesList.add(eaEdge);
      this.vertexList.add(eaEdge.getV0());
      this.vertexList.add(eaEdge.getV1());
    }
    return this;
  }

}
