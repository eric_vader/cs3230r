package com.e2empire.data.graph;

import java.security.InvalidParameterException;

/**
 * Defines a relation between 2 vertex, v0 and v1. it does not distinguish the direction of the
 * edge. This data object is immutable.
 *
 * @author Han Liang Wee, Eric
 */
public class Edge {

  private Vertex<?> v0;
  private Vertex<?> v1;

  /**
   * Creates an edge between v0 and v1.
   *
   * @param v0 one of the vertex.
   * @param v1 another vertex.
   * @exception InvalidParameterException the edge is invalid; cannot be null, cannot have the same
   *            value but not the same object.
   */
  public Edge(Vertex<?> v0, Vertex<?> v1) {

    if ((v0 == null) || (v1 == null)) {
      throw new InvalidParameterException("The vertices cannot be null.");
    }

    if (v0.equals(v1) && (v0 != v1)) {
      throw new InvalidParameterException("Each vertex must be unique.");
    }

    this.v0 = v0;
    this.v1 = v1;
  }

  /**
   * @return The v0.
   */
  public Vertex<?> getV0() {
    return this.v0;
  }

  /**
   * @param v0 The v0 to set.
   */
  public void setV0(Vertex<?> v0) {
    this.v0 = v0;
  }

  /**
   * @return The v1.
   */
  public Vertex<?> getV1() {
    return this.v1;
  }

  /**
   * @param v1 The v1 to set.
   */
  public void setV1(Vertex<?> v1) {
    this.v1 = v1;
  }

  /**
   * Check if the edge contains the particular vertex.
   *
   * @param vertex the vertex that is to be checked.
   * @return <code>true</code> if the vertex is one of the vertices in the edge.
   */
  public boolean contains(Vertex<?> vertex) {
    return (this.v0.equals(vertex) || this.v1.equals(vertex));
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Edge)) {
      return false;
    }
    Edge objEdge = (Edge) obj;
    // same direction
    return (objEdge.v0.equals(this.v0) && objEdge.v1.equals(this.v1))
        || (objEdge.v0.equals(this.v1) && objEdge.v1.equals(this.v0));
  }

  @Override
  public int hashCode() {
    return this.v0.hashCode() + this.v1.hashCode() + this.v1.hashCode();
  }

  @Override
  public String toString() {
    return String.format("%s->%s", this.v0, this.v1);
  }

}
