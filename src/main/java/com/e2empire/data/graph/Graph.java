package com.e2empire.data.graph;

import org.apache.commons.collections4.map.MultiValueMap;

import com.googlecode.javaewah.EWAHCompressedBitmap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Defines a Graph with Vertices and Edges.
 *
 * @author Han Liang Wee, Eric
 */
public class Graph {

  private Vertex<?>[] backingArray;
  private Edge[] backingEdges;
  private HashSet<Vertex<?>> validVerticesSet;
  // lookup the list of vertices connected to the key of the lookup.
  private MultiValueMap<Vertex<?>, Vertex<?>> lookupNeighbours;
  // retrieves the index location of the vertex in the backing array.
  private Map<Vertex<?>, Integer> reverseIndexLookup;

  /**
   * Creates a graph with the following edges, the vertices will be added automatically from the set
   * of edges.
   *
   * @param edgesArray Edges that are present in the graph.
   * @param vertexArray Array of vertices.
   */
  Graph(Vertex<?>[] vertexArray, Edge[] edgesArray) {

    this.backingArray = vertexArray;
    this.backingEdges = edgesArray;
    this.validVerticesSet = new HashSet<Vertex<?>>();
    this.reverseIndexLookup = new HashMap<Vertex<?>, Integer>();

    int temp = 0;
    for (Vertex<?> eaVertex : this.backingArray) {
      this.validVerticesSet.add(eaVertex);
      this.reverseIndexLookup.put(eaVertex, temp);
      temp++;
    }
    if (this.validVerticesSet.size() != vertexArray.length) {
      throw new IllegalStateException("Vertices in the vertex array must be unique.");
    }

    // in this lookup, if the edges are not connected to anything they will be null.
    this.lookupNeighbours = new MultiValueMap<>();

    // this set is to remember traversed nodes
    for (Edge eaEdge : edgesArray) {

      if (!this.validVerticesSet.contains(eaEdge.getV0())) {
        throw new IllegalStateException(
            "Must initialize the vertex Array with all the possible vertices in the graph.");
      }

      if (!this.validVerticesSet.contains(eaEdge.getV1())) {
        throw new IllegalStateException(
            "Must initialize the vertex Array with all the possible vertices in the graph.");
      }

      if (!eaEdge.getV0().equals(eaEdge.getV1())) {
        this.lookupNeighbours.put(eaEdge.getV0(), eaEdge.getV1());
      }
      this.lookupNeighbours.put(eaEdge.getV1(), eaEdge.getV0());

    }
  }

  /**
   * @return the backingArray.
   */
  public Vertex<?>[] getBackingArray() {
    return this.backingArray;
  }

  /**
   * @return the backingEdges.
   */
  public Edge[] getBackingEdges() {
    return this.backingEdges;
  }

  /**
   * Get an empty set of vertices.
   *
   * @return the Empty set of vertices.
   */
  public Vertices getEmptyVertices() {
    return new Vertices();
  }

  /**
   * Retrieves the set that contains the vertex specified.
   *
   * @param vertexArray The array of vertex that will be used to create the vertices.
   * @return the Vertices that contain all the vertices in the vertexArray.
   */
  public Vertices getVertices(Vertex<?>... vertexArray) {

    if ((vertexArray == null) || (vertexArray.length == 0)) {
      return new Vertices();
    }

    int[] indexes = new int[vertexArray.length];
    HashSet<Vertex<?>> traversedVerticesSet = new HashSet<Vertex<?>>();
    int temp = 0;
    for (Vertex<?> eaVertex : vertexArray) {

      // check if in the graph
      if (!this.validVerticesSet.contains(eaVertex)) {
        throw new IllegalStateException(
            "Cannot get vertices that are not initialized in the graph.");
      }

      // check for duplicates
      if (traversedVerticesSet.contains(eaVertex)) {
        throw new IllegalStateException("Cannot have repeated vertices in the array.");
      }
      traversedVerticesSet.add(eaVertex);

      // add to indexes
      indexes[temp] = this.reverseIndexLookup.get(eaVertex);
      temp++;

    }

    return new Vertices(EWAHCompressedBitmap.bitmapOf(indexes));
  }

  /**
   * Retrieves the neighbor of the specified vertex.
   *
   * @param vertex The vertex for which the neighbors will be retrieved.
   * @return The vertices that are neighbors of the vertex.
   */
  public Vertices getNeighbourOf(Vertex<?> vertex) {
    Collection<Vertex<?>> vertexCollection = this.lookupNeighbours.getCollection(vertex);
    if (vertexCollection != null) {
      return this.getVertices(vertexCollection.toArray(new Vertex<?>[vertexCollection.size()]));
    } else {
      return new Vertices();
    }
  }

  /**
   * Defines the vertices in a graph, defined not as a separate class because it can rely on some
   * properties of the current graph. Note that the set of vertices is immutable.
   *
   * @author Han Liang Wee, Eric
   */
  public class Vertices
      implements com.e2empire.data.graph.Set<Vertex<?>, Vertices>, Comparable<Vertices> {

    EWAHCompressedBitmap bitmap;
    private int size;

    /**
     * Creates an non empty Vertices set.
     *
     * @param resultBitmap the backing bitmap to back the vertices object.
     */
    private Vertices(EWAHCompressedBitmap resultBitmap) {
      this.bitmap = resultBitmap;
    }

    /**
     * Creates a fake Vertices, just to use for comparator.
     *
     * @param size size to set.
     */
    public Vertices(int size) {
      this.size = size;
    }

    /**
     * Creates an empty Vertices set.
     */
    public Vertices() {
      this.bitmap = new EWAHCompressedBitmap();
    }

    @Override
    public Vertices union(Vertices toUnionWith) {
      return new Vertices(this.bitmap.or(toUnionWith.bitmap));

    }

    @Override
    public Vertices intersect(Vertices toIntersectWith) {
      return new Vertices(this.bitmap.and(toIntersectWith.bitmap));
    }

    @Override
    public Vertices complement(Vertices toSubtract) {
      return new Vertices(this.bitmap.andNot(toSubtract.bitmap));
    }

    @Override
    public Vertices cartesianProduct(Vertices toMultiplyWith) {
      throw new UnsupportedOperationException(
          "This operation does not apply for the set of vertices.");
    }

    @Override
    public boolean isEmpty() {
      return this.bitmap.isEmpty();
    }

    @Override
    public Iterator<Vertex<?>> iterator() {
      return this.toList().iterator();
    }

    @Override
    public List<Vertex<?>> toList() {
      List<Integer> positions = this.bitmap.toList();
      List<Vertex<?>> vertices = new ArrayList<Vertex<?>>();
      for (Integer eaPosition : positions) {
        vertices.add(Graph.this.backingArray[eaPosition]);
      }
      return vertices;
    }

    @Override
    public String toString() {
      List<Vertex<?>> vertexArray = new ArrayList<>();
      for (Integer eaIndex : this.bitmap) {
        vertexArray.add(Graph.this.backingArray[eaIndex]);
      }
      return vertexArray.toString();
    }

    @Override
    public int size() {
      if (this.bitmap == null) {
        return this.size;
      } else {
        return this.bitmap.cardinality();
      }
    }

    private Graph getOuterType() {
      return Graph.this;
    }

    @Override
    public int compareTo(Vertices other) {
      return Integer.compare(this.hashCode(), other.hashCode());
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Graph [backingArray=" + Arrays.toString(this.backingArray) + ", validVerticesSet="
        + this.validVerticesSet + ", lookupConnectedVertices=" + this.lookupNeighbours
        + ", reverseIndexLookup=" + this.reverseIndexLookup + "]";
  }

  /**
   * Defines the Edges in a graph, similarly, defined here as there is some contextual meaning.
   *
   * @author Han Liang Wee, Eric
   */
  public class Edges {

  }

  /**
   * Retrieves all the vertices in the graph.
   *
   * @return the Vertices that contain all the vertices in the graph.
   */
  public Vertices getAllVertices() {
    return this.getVertices(this.backingArray);
  }

  /**
   * Returns the number of edges for the mentioned vertex.
   *
   * @param vertex the vertex to retrieve the number of edges.
   * @return the number of edges that concerns the vertex.
   */
  public int getNumberOfEdges(Vertex<?> vertex) {
    Collection<Vertex<?>> neighbours = this.lookupNeighbours.getCollection(vertex);
    if (neighbours != null) {
      return neighbours.size();
    } else {
      return 0;
    }
  }
}
