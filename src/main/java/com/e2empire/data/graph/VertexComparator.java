package com.e2empire.data.graph;

import java.util.Comparator;

/**
 * A vertex Comparator, to compare 2 vertex. Sort the 2 vertex based on the number of edges.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class VertexComparator implements Comparator<Vertex<?>> {

  private Graph graph;

  /**
   * Initializes the Comparator with the graph that is backing it.
   *
   * @param graph the graph that will be used to get the number of edges for each vertex.
   */
  public VertexComparator(Graph graph) {
    this.graph = graph;
  }

  @Override
  public int compare(Vertex<?> o1, Vertex<?> o2) {
    int o1NoEdges = this.graph.getNumberOfEdges(o1);
    int o2NoEdges = this.graph.getNumberOfEdges(o2);
    return Integer.compare(o1NoEdges, o2NoEdges);
  }

}
