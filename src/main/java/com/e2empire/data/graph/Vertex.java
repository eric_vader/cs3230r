package com.e2empire.data.graph;

/**
 * Defines a Vertex of a graph, a single node.
 *
 * @author Han Liang Wee, Eric
 * @param <T> underlying type backing this vertex, which must be unique.
 */
public class Vertex<T extends Comparable<T>> implements Comparable<Vertex<T>> {

  private T element;

  /**
   * Creates a new Vertex with
   *
   * @param backingElement the element backing this Vertex.
   */
  public Vertex(T backingElement) {
    this.element = backingElement;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof Vertex)) {
      return false;
    }
    Vertex<?> objVertex = (Vertex<?>) obj;
    return this.element.equals(objVertex.element);
  }

  @Override
  public int hashCode() {
    return this.element.hashCode();
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    return "Vertex[" + this.element + "]";
  }

  /**
   * @return the element.
   */
  public T getElement() {
    return this.element;
  }

  /**
   * @param element the element to set.
   */
  public void setElement(T element) {
    this.element = element;
  }

  @Override
  public int compareTo(Vertex<T> o) {
    return this.element.compareTo(o.element);
  }

}
