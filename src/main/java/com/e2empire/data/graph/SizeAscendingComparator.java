package com.e2empire.data.graph;

import com.e2empire.data.graph.GraphTree.Vertices;

import java.util.Comparator;

/**
 * Orders the Vertex by Size of the vertices.
 *
 * @author Han Liang Wee, Eric
 *
 */
public class SizeAscendingComparator implements Comparator<Vertex<Vertices>> {

  @Override
  public int compare(Vertex<Vertices> o1, Vertex<Vertices> o2) {
    return Integer.compare(o1.getElement().size(), o2.getElement().size());
  }

}

